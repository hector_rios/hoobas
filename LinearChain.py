#coding: utf-8
import copy
import random
import os
import subprocess
from math import *
try:
    import DNA3SPN.make_bp_params
    import DNA3SPN.pdb2cg_dna
except ImportError:
    print 'Error while importing DNA3SPN subpackages, LinearChain will be unable to build this DNA model'

import numpy as np

import CoarsegrainedBead
from Util import get_v1_v2_rotmat as v1v2rmat
from Util import Vector as vec
from Units import SimulationUnits as SimUnits
import Util


class LinearChain(object):
    """
    This is a class for linear chains in the model. Subclasses are expected to give constants for angles and other funct
    ions. initial chain is expected to run along Z, while DNA is added on x-y plane

    Properties :

    zero_pos : position of the first bead in the chain

    center_position : position of the center of the chain, calculated by arithmetic average

    bond_types : returns the bond types in the chain in the form [['name', k, r0], ... ]

    angle_types : returns the angle types in the chain in the form [['name', k, t0], ...]

    dihedral_types : returns the dihedral types in the form [['name', params0, params1, ... ], ...], parameter number
     depends on the potential type, OPLS has 4

    pnum_offset : shifts all bead numbers in potentials by this constant

    Methods :

    add_dna(#, n_ss, n_ds, sticky_end) : adds DNA defined by the parameters to the linear chain. DNA are grafted to
    remaining sites

    randomize_dirs : randomizes the chain directions

    change_remaining_att_sites(key_search, max_num_bindings = 1) : changes the remaining attachment sites in the chain
    by using a key search in the beads. the key is a dictionary (e.g. {'beadtype':'P'}). max_num_bindings is the maximum
    number of grafts a site can have, including all previous bindings

    graft_N_ext_obj(N, obj, connecting_bond = None, add_attachment_sites = False) : grafts N copies (N<remaining sites)
    of obj to the chain, connecting the newly added chain by connecting_bond[0]. If add_attachment_sites is true, the
    grafted object attachment sites will be appended to the remaining sites

    random_roration():
    Rotates the chain by a random matrix


    """
    def __init__(self, n_monomer=None, kuhn_length=None, units=None):

        self.nmono = n_monomer
        self.lmono = kuhn_length
        self.positions = np.zeros((0, 3), dtype=np.float32)
        self.mass = []

        self.pnum = []

        self.beads = []

        self.bonds = []
        self.angles = []
        self.dihedrals = []
        self.impropers = []

        self.b_types = []
        self.a_types = []
        self.d_types = []
        self.i_types = []

        self.types = []
        self.wdv = []
        self.dir = [0.0, 0.0, 1.0]
        self.rem_sites = []
        self.att_sites = []
        self.sticky_used = []

        self.chain_name = 'empty'

        # set units
        if units is not None and issubclass(units.__class__, SimUnits):
            self.units = units
        else:
            self.units = SimUnits()

    @staticmethod
    def get_rot_mat(cubic_plane):
        _cp = vec(cubic_plane)
        _cp.array /= _cp.norm()

        _v = _cp.cross_product_with_z_axis()
        _sl = _v.norm()
        _cl = _cp.i_prod_by001()

        _mat_vx = np.array([[0.0, -_v.z, _v.y], [_v.z, 0.0, -_v.x], [-_v.y, _v.x, 0.0]])
        _id = np.array([[1.0, 0, 0], [0, 1.0, 0], [0, 0, 1.0]])

        if _v.array[0] == 0.0 and _v.array[1] == 0.0:  # surface is [001]
            return _id
        return _id + _mat_vx + (1.0-_cl) / _sl**2 * np.linalg.matrix_power(_mat_vx, 2)

    @staticmethod
    def random_rot_matrix():
        _r_th = random.uniform(0, 2*pi)
        _r_z = random.uniform(0, 1)
        _r_ori = [(1-_r_z**2)**0.5*cos(_r_th), (1-_r_z**2)**0.5*sin(_r_th), _r_z]
        return LinearChain.get_rot_mat(_r_ori)

    def random_rotation(self):
        _r_mat = self.random_rot_matrix()
        for i in range(self.beads.__len__()):
            _dmp_vec = vec(self.beads[i].position)
            _dmp_vec.rotate(mat=_r_mat)
            self.beads[i].position = _dmp_vec.array
            del _dmp_vec
        _dmp_vec = vec(self.dir)
        _dmp_vec.rotate(mat=_r_mat)
        self.dir = _dmp_vec.array
        del _dmp_vec

    def __str__(self):
        return self.chain_name

    def set_name(self, name):
        self.chain_name = str(name)

    def rotate(self, direction):
        rmat = v1v2rmat(self.dir, direction)
        for idx in range(0, self.beads.__len__()):
            self.beads[idx].position = np.dot(self.beads[idx].position, rmat)
            self.positions[idx] = np.dot(self.positions[idx], rmat)

    def XY_rotate(self, angle):
        rmat = np.array([[cos(angle), -sin(angle), 0.0], [sin(angle), cos(angle), 0.0], [0.0, 0.0, 1.0]])
        for idx in range(0, self.beads.__len__()):
            self.beads[idx].position = np.dot(self.beads[idx].position, rmat)
            self.positions[idx] = np.dot(self.positions[idx], rmat)

    def type_by_idx(self, idx):
        try:
            return self.beads[idx].beadtype
        except IndexError:
            return None

    @property
    def zero_pos(self):
        return self.positions[0, :]

    @zero_pos.setter
    def zero_pos(self, val):
        diff = val - self.positions[0, :]
        self.positions += diff
        for i in range(self.beads.__len__()):
            self.beads[i].position += diff

    @property
    def center_position(self):
        _ret = np.zeros(3, dtype=float)
        for i in range(self.beads.__len__()):
            _ret += self.beads[i].position
        return _ret / self.beads.__len__()

    @center_position.setter
    def center_position(self, val):
        diff = val - self.center_position
        for i in range(self.beads.__len__()):
            self.beads[i].position += diff

    @property
    def bond_types(self):
        return self.b_types

    @property
    def angle_types(self):
        return self.a_types

    @property
    def dihedral_types(self):
        return self.d_types

    @property
    def pnum_offset(self):
        return self.pnum[0]

    @pnum_offset.setter
    def pnum_offset(self, val):
        diff = val - self.pnum[0]

        for i in range(self.pnum.__len__()):
            self.pnum[i] += diff
        for i in range(self.bonds.__len__()):
            for j in range(1, self.bonds[i].__len__()):
                self.bonds[i][j] += diff
        for i in range(self.angles.__len__()):
            for j in range(1, self.angles[i].__len__()):
                self.angles[i][j] += diff
        for i in range(self.dihedrals.__len__()):
            for j in range(1, self.dihedrals[i].__len__()):
                self.dihedrals[i][j] += diff

    def add_dna(self, num, n_ss, n_ds, sticky_end):
        """
        Adds DNA to the linear chain
        :param num: # of DNA chains added
        :param n_ss: # of single stranded beads in the chain
        :param n_ds: # of double stranded beads in the chain
        :param sticky_end: sticky end beads (e.g., ['X', 'Y'])
        :return: None
        """

        if num > self.rem_sites.__len__():
            raise StandardError('# of DNA chains greater than # of attachment sites on Linear Chain')
        dna_obj = DNAChain(n_ss=n_ss, bond_length=0.6, n_ds=n_ds, sticky_end=sticky_end)
        dna_obj.change_units(new_units=self.units)
        if num > 0:
            self.sticky_used.append(sticky_end)
        for i in range(num):
            _dump_copy = copy.deepcopy(dna_obj)
            for j in range(_dump_copy.beads_in_one_dna_chain.__len__()):
                _dump_copy.beads_in_one_dna_chain[j].position[2] += 0.2
            _dump_copy.randomize_dirs()
            _p_off = self.pnum[-1]+1
            self.att_sites.append(self.rem_sites.pop(random.randint(0, self.rem_sites.__len__()-1)))
            _attvec = vec(self.positions[self.att_sites[-1], :])
            _rotmat = self.get_rot_mat([random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-0.1, 0.1)])

            for j in range(_dump_copy.beads_in_one_dna_chain.__len__()):
                _v = vec(_dump_copy.beads_in_one_dna_chain[j].position)
                _v.rotate(mat=_rotmat)
                _dump_copy.beads_in_one_dna_chain[j].position = _v.array + self.positions[self.att_sites[-1], :]
                del _v
                self.positions = np.append(self.positions, np.array([_dump_copy.beads_in_one_dna_chain[j].position[:]]),
                                           axis=0)
                self.beads.append(_dump_copy.beads_in_one_dna_chain[j])
                self.pnum.append(_p_off+j)
            for j in range(_dump_copy.harmonic_bonds_in_one_dna_chain.__len__()):
                _dump_copy.harmonic_bonds_in_one_dna_chain[j][1] += _p_off
                _dump_copy.harmonic_bonds_in_one_dna_chain[j][2] += _p_off
                self.bonds.append(_dump_copy.harmonic_bonds_in_one_dna_chain[j])
            self.bonds.append(['S-NP', self.att_sites[-1], _p_off])
            for j in range(_dump_copy.angles_in_one_dna_chain.__len__()):
                for k in range(_dump_copy.angles_in_one_dna_chain[j].__len__()-1):
                    _dump_copy.angles_in_one_dna_chain[j][k + 1] += _p_off
                self.angles.append(_dump_copy.angles_in_one_dna_chain[j])
            del _dump_copy, _attvec

    def randomize_dirs(self, tol=1e-1):
        _new_pos = np.zeros((self.beads.__len__(), 3), dtype=float)
        for i in range(1, self.beads.__len__()):
            _old_vec = vec(np.array(self.beads[i].position) - np.array(self.beads[i-1].position))
            _old_vec2 = vec(np.array(self.beads[i].position) - np.array(self.beads[i-1].position))
            _r_mat = self.random_rot_matrix()
            _old_vec2.rotate(_r_mat)
            _new_vec = vec(_old_vec.array * (1-tol) + _old_vec2.array * tol)
            _new_pos[i, :] = _new_pos[i-1, :] + _new_vec.array
        for i in range(1, self.beads.__len__()):
            self.beads[i].position = _new_pos[i, :]

    def change_remaining_att_sites(self, key_search, max_num_bindings=1):
        self.rem_sites = []
        for i in range(self.beads.__len__()):
            _bind_counter = 0
            for j in self.att_sites.__len__():
                if self.pnum[i] == self.att_sites[j]:
                    _bind_counter += 1
            if _bind_counter < max_num_bindings:
                _bool_add = True
                for key in key_search.keys():
                    try:
                        _bool_add = _bool_add and self.beads.__getattribute__(key) == key_search[key]
                    except AttributeError:
                        _bool_add = False
                        break
                if _bool_add:
                    for _diff_binding_num in range(max_num_bindings - _bind_counter):
                        self.rem_sites.append(self.pnum[i])

    def graft_N_ext_object(self, obj, N, connecting_bond=None, add_attachment_sites=False):
        _graft_att_sites = []
        if connecting_bond is None:
            connecting_bond = ['Chain-Graft', 330.0, 1.0]
        if N > self.rem_sites.__len__():
            raise StandardError('Number of grafted chains greater than attachments sites')
        for i in range(N):
            _dmp_copy = copy.deepcopy(obj)
            _dmp_copy.change_units(new_units=self.units)
            try:
                _dmp_copy.randomize_dirs()
            except NameError:
                pass
            _p_off = self.pnum[-1]+1
            self.att_sites.append(self.rem_sites.pop(random.randint(0, self.rem_sites.__len__()-1)))
            _attvec = vec(self.positions[self.att_sites[-1], :])
            _rotmat = self.get_rot_mat([random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1)])

            for j in range(_dmp_copy.beads.__len__()):
                _v = vec(_dmp_copy.beads[j].position)
                _v.rotate(mat=_rotmat)
                _dmp_copy.beads[j].position = _v.array + self.positions[self.att_sites[-1], :] + np.random.uniform(0, 1e-2, 3)
                del _v
                self.positions = np.append(self.positions, np.array([_dmp_copy.beads[j].position[:]]), axis=0)
                self.beads.append(_dmp_copy.beads[j])
                self.pnum.append(_p_off+j)
            for j in range(_dmp_copy.bonds.__len__()):
                _dmp_copy.bonds[j][1] += _p_off
                _dmp_copy.bonds[j][2] += _p_off
                self.bonds.append(_dmp_copy.bonds[j])
            self.bonds.append([connecting_bond[0], self.att_sites[-1], _p_off])
            for j in range(_dmp_copy.angles.__len__()):
                for k in range(_dmp_copy.angles[j].__len__()-1):
                    _dmp_copy.angles[j][k+1] += _p_off
                self.angles.append(_dmp_copy.angles[j])
            for j in range(_dmp_copy.dihedrals.__len__()):
                for k in range(_dmp_copy.dihedrals[j].__len__()):
                    _dmp_copy.dihedrals[j][k+1] += _p_off
                self.dihedrals.append(_dmp_copy.dihedrals[j])
            if add_attachment_sites:
                try:
                    for j in range(1, _dmp_copy.rem_sites.__len__()):
                        _graft_att_sites.append(_dmp_copy.rem_sites[j] + _p_off)
                except NameError:
                    pass
            del _dmp_copy, _attvec
        if add_attachment_sites:
            self.rem_sites.append(_graft_att_sites)
        self.b_types += obj.bond_types
        self.b_types += connecting_bond
        self.a_types += obj.angle_types
        self.d_types += obj.dihedral_types

    def change_units(self, new_units):
        # have to change positions, bead list properties, masses, bond / angle / dihedral / wdv type listings.

        if new_units is None:
            return
        if not issubclass(new_units.__class__, SimUnits):
            raise SyntaxError('Units supplied to LinearChain instance is not a subclass of SimulationUnits')

        if new_units.lunit != self.units.lunit:
            m_len = new_units.get_length(1.0, self.units.lunit)
            if hasattr(self.lmono, '__iter__'):
                for k_l in self.lmono:
                    k_l *= m_len
            else:
                self.lmono *= m_len
            for p in self.positions:
                p *= m_len
            for bond in self.b_types:
                bond[2] *= m_len
            for force in self.wdv:
                force[3] *= m_len
        else:
            m_len = 1.0

        if new_units.munit != self.units.munit:
            m_mass = new_units.get_mass(1.0, self.units.munit)
            for m in self.mass:
                m *= m_mass
        else:
            m_mass = 1.0

        if new_units.Eunit != self.units.Eunit:
            E_mul = new_units.get_energy(1.0, self.units.Eunit)
            for bond in self.b_types:
                bond[1] *= E_mul
            for ang in self.a_types:
                ang[1] *= E_mul
            for die in self.d_types:
                if die.__len__() == 5:
                    for el_idx in range(1, die.__len__()):
                        die[el_idx] *= E_mul
                elif die.__len__() == 3:
                    die[1] *= E_mul
            for force in self.wdv:
                force[3] *= E_mul
        else:
            E_mul = 1.0
        for bead in self.beads:
            bead.change_units(m_mass, m_len, E_mul)
        self.units = new_units

    def do_on_copy(self): # virtual method for special cases where copying objects needs to do something (increment molecule index or whatever)
        pass

    def reverse_order(self):
        # reverses all chain order
        self.rotate(direction=[-self.dir[0], -self.dir[1], - self.dir[2]])
        self.dir = [-self.dir[0], -self.dir[1], -self.dir[2]]
        # use the inverted order as index
        _ibeads = range(self.beads.__len__())
        _ibeads = _ibeads[::-1]
        self.positions = self.positions[::-1, :]
        for bond in self.bonds:
            bond[1] = _ibeads[bond[1]]
            bond[2] = _ibeads[bond[2]]
        for angle in self.angles:
            for item in range(1, angle.__len__()):
                angle[item] = _ibeads[angle[item]]
        for dihedral in self.dihedrals:
            for item in range(1, dihedral.__len__()):
                dihedral[item] = _ibeads[dihedral[item]]
        for dihedral in self.impropers:
            for item in range(1, dihedral.__len__()):
                dihedral[item] = _ibeads[dihedral[item]]
        self.beads = self.beads[::-1]
        self.pnum = self.pnum[::-1]


class Polyaniline(LinearChain):

    def __init__(self, n_monomer=None):
        # units are standard reduced 'R'
        LinearChain.__init__(self, n_monomer=n_monomer, kuhn_length=[5.56/20, 5.52/20, 5.47/20])

        self.b_types = [['paA-paA', 71.84 * 4.18 * (20**2), 5.56/20], ['paA-paB', 111.28 * 4.18 * 20**2, 5.52/20],
                        ['paB-paB', 143.03 * 4.18 * 20**2, 5.47/20]]
        self.a_types = [['paA-paA-paB', 19.46*4.18*sin(1.94)**2, 1.94], ['paA-paB-paB', 38.80*4.18*sin(2.45)**2, 2.45]]
        self.d_types = [['paA-paA-paB-paB', 0.5*0.8*4.18, 0.5*0.73*4.18, 0.5*-0.03*4.18, 0.5*0.22*4.18],
                        ['paA-paB-paB-paA', 0.5*0.66*4.18, 0.5*-0.54*4.18, 0.5*-0.1*4.18, 0.5*0.02*4.18],
                        ['paB-paA-paA-paB', 0.5*0.45*4.18, 0.5*-1.33*4.18, 0.5*0.13*4.18, 0.5*0.67*4.18]]
        self.wdv = [['paA', 'paA', 0.32*4.18, 5.14/20], ['paB', 'paB', 0.34*4.18, 5.14 / 20],
                    ['paA', 'paB', (0.32*0.34)**0.5*4.18, 5.14/20]]

        self.mass = [163.0/815, 162.0 / 815]

        self.__build_chain()
        self.__build_chain_beads()
        self.randomize_dirs()
        self.rem_sites = range(0, self.beads.__len__(), 4)+range(3, self.beads.__len__(), 4)  # sites available

    def __build_chain(self):
        self.positions = np.append(self.positions, [[0.0, 0.0, 0.0]], axis=0)
        self.positions = np.append(self.positions, [[0.0, 0.0, self.lmono[1]]], axis=0)
        self.positions = np.append(self.positions, [[0.0, 0.0, self.lmono[1]+self.lmono[2]]], axis=0)
        self.positions = np.append(self.positions, [[0.0, 0.0, self.lmono[1]+self.lmono[2]+self.lmono[2]]], axis=0)

        self.bonds.append(['paA-paB', 0, 1])
        self.bonds.append(['paB-paB', 1, 2])
        self.bonds.append(['paA-paB', 2, 3])

        self.angles.append(['paA-paB-paB', 0, 1, 2])
        self.angles.append(['paA-paB-paB', 1, 2, 3])

        self.dihedrals.append(['paA-paB-paB-paA', 0, 1, 2, 3])

        self.pnum = [0, 1, 2, 3]

        # struct ia ABBA type repeated
        for i in range(self.nmono-1):

            self.positions = np.append(self.positions, [[0.0, 0.0, self.positions[-1, 2] + self.lmono[0]]], axis=0)
            self.pnum.append(self.pnum[-1] + 1)
            self.bonds.append(['paA-paA', self.pnum[-1], self.pnum[-2]])
            self.angles.append(['paA-paA-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3]])
            self.dihedrals.append(['paA-paA-paB-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3], self.pnum[-4]])

            self.positions = np.append(self.positions, [[0.0, 0.0, self.positions[-1, 2] + self.lmono[1]]], axis=0)
            self.pnum.append(self.pnum[-1] + 1)
            self.bonds.append(['paA-paB', self.pnum[-1], self.pnum[-2]])
            self.angles.append(['paA-paA-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3]])
            self.dihedrals.append(['paB-paA-paA-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3], self.pnum[-4]])

            self.positions = np.append(self.positions, [[0.0, 0.0, self.positions[-1, 2] + self.lmono[2]]], axis=0)
            self.pnum.append(self.pnum[-1] + 1)
            self.bonds.append(['paB-paB', self.pnum[-1], self.pnum[-2]])
            self.angles.append(['paA-paB-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3]])
            self.dihedrals.append(['paA-paA-paB-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3], self.pnum[-4]])

            self.positions = np.append(self.positions, [[0.0, 0.0, self.positions[-1, 2] + self.lmono[1]]], axis=0)
            self.pnum.append(self.pnum[-1] + 1)
            self.bonds.append(['paA-paB', self.pnum[-1], self.pnum[-2]])
            self.angles.append(['paA-paB-paB', self.pnum[-1], self.pnum[-2], self.pnum[-3]])
            self.dihedrals.append(['paA-paB-paB-paA', self.pnum[-1], self.pnum[-2], self.pnum[-3], self.pnum[-4]])

    def __build_chain_beads(self):
        for i in range(self.positions.__len__()):
            if not (i % 4) or (i % 4 == 3):
                self.beads.append(CoarsegrainedBead.bead(position=self.positions[i], beadtype='paA', mass=self.mass[0],
                                                         charge=0, body=-1))
            else:
                self.beads.append(CoarsegrainedBead.bead(position=self.positions[i], beadtype='paB', mass=self.mass[0],
                                                         charge=0, body=-1))


class GenericPolymer(LinearChain):

    def __init__(self, n_mono=100, kuhn_length=1.0, beadname=None, rigid=False):
        LinearChain.__init__(self, n_monomer=n_mono, kuhn_length=kuhn_length)

        if beadname is None:
            self.BeadGenericName = 'GenericPolymer'
        else:
            self.BeadGenericName = str(beadname)

        self.wdv = [[self.BeadGenericName, self.BeadGenericName, 1.0, kuhn_length]]
        self.b_types = [[self.BeadGenericName+'Backbone', 300.0, self.lmono]]

        self.__build_chain()
        self.__build_beads()
        if rigid:
            self.__build_angles()
        self.randomize_dirs()
        self.rem_sites = range(0, self.positions.__len__())

    def __build_chain(self):
        self.positions = np.append(self.positions, [[0.0, 0.0, 0.0]], axis=0)
        self.pnum = [0]
        self.mass = [1.0]

        for i in range(self.nmono - 1):
            self.positions = np.append(self.positions, [[0.0, 0.0, self.positions[-1, 2] + self.lmono]], axis=0)
            self.pnum.append(self.pnum[-1]+1)
            self.bonds.append([self.BeadGenericName+'Backbone', self.pnum[-1], self.pnum[-2]])

    def __build_beads(self):
        for i in range(self.positions.__len__()):
            self.beads.append(CoarsegrainedBead.bead(position=self.positions[i, :], beadtype=self.BeadGenericName, body=-1))

    def __build_angles(self):
        for i in range(self.nmono - 2):
            self.angles.append([self.BeadGenericName + 'BackboneAngle', self.pnum[i], self.pnum[i+1], self.pnum[i+2]])


class GenericRingPolymer(LinearChain):

    def __init__(self, n_mono=100, kuhn_length=1.0, beadname=None, rigid=False):
        LinearChain.__init__(self, n_monomer=n_mono, kuhn_length=kuhn_length)

        if beadname is None:
            self.BeadGenericName = beadname
        else:
            self.BeadGenericName = str(beadname)

        self.wdv = [[self.BeadGenericName, self.BeadGenericName, 1.0, kuhn_length]]
        self.b_types = [[self.BeadGenericName+'Backbone', 300.0, self.lmono]]

        self.__build_chain()
        self.__build_beads()
        if rigid:
            self.__build_angles()
        self.randomize_dirs()
        self.rem_sites = range(0, self.positions.__len__())

        self.R = self.lmono * self.nmono / (2*pi)

    def __build_chain(self):
        self.positions = np.append(self.positions, [[self.R, 0.0, 0.0]], axis=0)
        self.pnum = [0]
        self.mass = [1.0]

        for i in range(self.nmono - 1):
            self.positions = np.append(self.positions, [[self.R * cos(2*pi * (i+1) / (self.nmono+1)),
                                                         self.R * sin(2*pi * (i+1) / (self.nmono+1)), 0.0]])
            self.pnum.append(self.pnum[-1]+1)
            self.bonds.append([self.BeadGenericName+'Backbone', self.pnum[-1], self.pnum[-2]])
        self.bonds.append([self.BeadGenericName+'Backbone', self.pnum[-1], self.pnum[0]])

    def __build_beads(self):
        for i in range(self.positions.__len__()):
            self.beads.append(CoarsegrainedBead.bead(position=self.positions[i, :], beadtype='GenericPolymer', body=-1))

    def __build_angles(self):
        for i in range(self.nmono):
            # pnum lists should range as [i, i+1, i+2, ..., i+self.nmono-1] so we contain overflow by % self.nmono
            self.angles.append([self.BeadGenericName+'BackboneAngle',
                                self.pnum[i],
                                (self.pnum[i+1] % self.nmono) + self.pnum[0],
                                (self.pnum[i+2] % self.nmono) + self.pnum[0]])


class RandomPolymer(LinearChain):
    """
    Class for polymers made of random sequences of monomers. Constructor takes a list of monomers as well as a pointer to
    a function that returns the selected monomer
    """
    def __init__(self, monomer_types, monomer_distributions, n_mono=100, bondtype_matrix=None, bondr0_matrix=None,
                 units=None):
        """
        Constructor for the random polymer
        :param monomer_types: list of available monomer types, e.g. ['A', 'B', another_chain] for a random sequence
        of 'A' and 'B' and another object, which implements __str__(). A constructor can be supplied for in place
        building to construct a random polymer of random polymer, in the form of {constructor, kwargs}, where
        constructor(**kwargs) is called. Bonds are applied to first bead of chain if the object doesnt imlement a
        rem_site method or variable.
        :param monomer_distributions: pointer to a function (needs a __call__ attribute) that takes in the current
        sequence of monomers and returns an index; e.g. a function that returns 0 75% of the time and 1 25% of the time
        will make a polymer made of 75% of tyoe 'A' and 25% of type 'B', assuming tha the monomer types are ['A', 'B']
        :param n_mono: number of monomers to create
        :param bondtype_matrix: list of bond types to create. Will default to monomer[i]-monomer[j] if nothing is
        supplied otherwise is of size monomer_types * monomer_types
        :param bondr0_matrix: list of bond distances defaults to 1.0, size monomer_types * monomer_types
        :param units: Simulation Units of the random polymer (useful if bondtypes are defined)
        :return:
        """
        if not hasattr(monomer_distributions, '__call__'):
            raise SyntaxError('The random distribution function is not callable')
        else:
            self.distr_func = monomer_distributions
        self.mtypes = monomer_types
        if hasattr(n_mono, '__call__'):
            nmono = n_mono.__call__()
        else:
            nmono = n_mono
        if bondr0_matrix is None:
            self.bondr0 = np.ones((monomer_types.__len__(), monomer_types.__len__()), dtype=np.float)
        else:
            self.bondr0 = np.array(bondr0_matrix, dtype=float)
            self.bondr0.reshape((monomer_types.__len__(), monomer_types.__len__()))
        if units is None:
            _units = SimUnits()
        else:
            _units = units
        super(RandomPolymer, self).__init__(n_monomer=nmono, kuhn_length=np.average(self.bondr0), units=_units)

        if bondtype_matrix is None:
            bname_list = [['' for i in range(monomer_types.__len__())] for j in range(monomer_types.__len__())]
            for idx1 in range(monomer_types.__len__()):
                for idx2 in range(idx1, monomer_types.__len__()):

                    if hasattr(monomer_types[idx1], '__iter__'):
                        name1 = str(monomer_types[idx1][0](**monomer_types[idx1][1]))
                    else:
                        name1 = str(monomer_types[idx1])
                    if hasattr(monomer_types[idx1], '__iter__'):
                        name2 = str(monomer_types[idx2][0](**monomer_types[idx2][1]))
                    else:
                        name2 = str(monomer_types[idx2])

                    bname_list[idx1][idx2] = name1 + '-' + name2
                    bname_list[idx2][idx1] = name1 + '-' + name2
            self.bondtype_matrix = np.array(bname_list)
        else:
            self.bondtype_matrix = np.array(bondtype_matrix)
            self.bondtype_matrix.reshape(monomer_types.__len__(), monomer_types.__len__())
        self.tidx_list = []
        self.polymer_topology = []
        self.monomeric_pnum = []

        # in case angles are added.  Rank3 tensor. Can have None values when no angles are to be added
        self.angletype_tensor = None

        # in case dihedrals are added Rank4 tensor. Can have None values when no dihedrals are to be added
        self.dihedraltype_tensor = None

        # in case impropers are added. Rank4 tensor. Can have None values
        self.impropertype_tensor = None

        self.__build_chain()

    # TODO : check time usage of this in-place construction to get string names.
    def get_m_name(self, idx):
        if hasattr(self.mtypes[idx], '__iter__'):
            return str(self.mtypes[idx][0](**self.mtypes[idx][1]))
        else:
            return str(self.mtypes[idx])

    def __build_chain(self):
        _pnum_link = 0
        while self.tidx_list.__len__() < self.nmono:
            # append new beads that we take from distribution
            tidx = self.distr_func(self.tidx_list)
            self.tidx_list.append(tidx)
            if isinstance(self.mtypes[tidx], str):  # usual chain construction
                if self.beads.__len__() == 0:
                    self.beads.append(CoarsegrainedBead.bead(position=np.array([0, 0, 0], dtype=float),
                                                             beadtype=self.mtypes[tidx]))
                    self.pnum.append(0)
                    self.monomeric_pnum.append([0])
                    self.types.append(self.mtypes[tidx])
                    self.positions = np.append(self.positions, [[0, 0, 0]], axis=0)
                    _pnum_link = 0
                else:
                    self.positions = np.append(self.positions, self.positions[-1, :] +
                                               [[0.0, 0.0, self.bondr0[tidx, self.tidx_list[-2]]]], axis=0)
                    self.beads.append(CoarsegrainedBead.bead(position=self.positions[-1, :], beadtype=self.mtypes[tidx]))
                    self.pnum.append(self.pnum[-1] + 1)
                    self.monomeric_pnum.append([self.pnum[-1] + 1])
                    self.types.append(self.mtypes[tidx])
                    self.bonds.append([self.bondtype_matrix[tidx, self.tidx_list[-2]], _pnum_link, self.pnum[-1]])
                    self.polymer_topology.append([self.tidx_list[-2], self.pnum[-1]])
                    _pnum_link = self.pnum[-1]
                continue
            # TODO : add try block and catch exceptions
            if hasattr(self.mtypes[tidx], '__iter__'):  # we have a full chain to append to the random polymer
                # construct the chain in-place
                obj_to_app = self.mtypes[tidx][0](**self.mtypes[tidx][1])
            else:
                obj_to_app = copy.deepcopy(self.mtypes[tidx])
            obj_to_app.change_units(new_units=self.units)
            # set the idx offset and the zero position
            if self.beads.__len__() != 0:
                obj_to_app.pnum_offset = self.pnum[-1] + 1
                if issubclass(obj_to_app.__class__, LinearChain):  # only linear chain have a 1D direction
                    obj_to_app.XY_rotate(random.uniform(0, 2 * pi))

                obj_to_app.zero_pos = (self.positions[-1, :] + [[0.0, 0.0, self.bondr0[tidx, self.tidx_list[-2]]]]).flatten()
                # link the object with the current chain
                if hasattr(obj_to_app, 'rem_sites'):  # has defined attachment sites
                    if hasattr(obj_to_app.rem_sites, '__call__'):  # is a function
                        s1 = obj_to_app.rem_sites() + obj_to_app.pnum_offset
                        self.bonds.append([self.bondtype_matrix[tidx, self.tidx_list[-2]], _pnum_link, s1])
                        self.polymer_topology.append([_pnum_link, s1])
                        _pnum_link = obj_to_app.rem_sites() + obj_to_app.pnum_offset
                    else:  # we have a list of attachment sites, pop one of them and bind to it
                        s1 = obj_to_app.rem_sites.pop(random.randint(0, obj_to_app.rem_sites.__len__() - 1)) + obj_to_app.pnum_offset
                        self.bonds.append([self.bondtype_matrix[tidx, self.tidx_list[-2]], _pnum_link, s1])
                        self.polymer_topology.append([_pnum_link, s1])
                        _pnum_link = obj_to_app.rem_sites.pop(random.randint(0, obj_to_app.rem_sites.__len__() - 1)) + obj_to_app.pnum_offset
                else:  # link to element 0
                    self.bonds.append([self.bondtype_matrix[tidx, self.tidx_list[-2]], _pnum_link, obj_to_app.pnum_offset])
                    self.polymer_topology.append([_pnum_link, obj_to_app.pnum_offset])
                    _pnum_link = obj_to_app.pnum_offset

            else:  # first monomer
                obj_to_app.pnum_offset = 0
                if hasattr(obj_to_app, 'rem_sites'):
                    if hasattr(obj_to_app.rem_sites, '__call__'):
                        _pnum_link = obj_to_app.rem_sites()
                    else:
                        _pnum_link = obj_to_app.rem_sites.pop(random.randint(0, obj_to_app.rem_sites.__len__() - 1))
                else:
                    _pnum_link = 0
            self.positions = np.append(self.positions, obj_to_app.positions, axis=0)
            self.pnum += obj_to_app.pnum
            self.monomeric_pnum += [obj_to_app.pnum]
            self.beads += obj_to_app.beads
            self.types += obj_to_app.types
            self.bonds += obj_to_app.bonds
            self.dihedrals += obj_to_app.dihedrals

        self.types = list(set(self.types))

    def add_backbone_angles(self, angletype_tensor=None):
        """
        Adds angle potentials to the backbone only; Automatic generation if no names are supplied. Angle potential is
        ignored if its name is None. Does NOT support this kind of configuration :

                 C
        A - A - X1
                B
                X2 - A - A - A
                C'

        See the add_angles() function for this kind of configuration

        :param angletype_tensor name tensor of rank 3 and dimension nmono x nmono x nmono
        :return:
        """
        # create tensors
        if angletype_tensor is None:
            atname = [[['' for i in range(self.mtypes.__len__())] for j in range(self.mtypes.__len__())]
                      for k in range(self.mtypes.__len__())]
            for idx1 in range(self.mtypes.__len__()):
                name1 = self.get_m_name(idx1)
                for idx2 in range(self.mtypes.__len__()):
                    name2 = self.get_m_name(idx2)
                    for idx3 in range(self.mtypes.__len__()):
                        name3 = self.get_m_name(idx3)
                        atname[idx1][idx2][idx3] = name1 + '-' + name2 + '-' + name3
            self.angletype_tensor = atname
        else:
            self.angletype_tensor = angletype_tensor

        # polymer topology contains the backbone listings.
        for a_idx in range(self.polymer_topology.__len__() - 1):
            idx1 = min(self.polymer_topology[a_idx])
            idx2 = max(self.polymer_topology[a_idx])
            idx3 = max(self.polymer_topology[a_idx + 1])
            aname = self.angletype_tensor[self.tidx_list[a_idx]][self.tidx_list[a_idx + 1]][self.tidx_list[a_idx + 2]]
            if aname is not None:
                self.angles.append([aname, idx1, idx2, idx3])

    def add_angles(self):
        """
        Adds angle potentials to the polymer. In the case of weird chains such as :

                C
           R1 - X1
                B
           B' - X2 - R2
                C'

        In which case the code will generate the angles :
        R1 - X1 - C
        R1 - X1 - B
        B - X2 - R2
        B' - X2 - R2
        C' - X2 - R2

        where R1 and R2 are the attachment points of the continuing chain

        Angle names are defined by beadtype, no angles will be skipped. Use other functions to remove angles
        :return:
        """
        for monomer_idx in range(0, self.nmono - 1):
            # this is the list of bonds [X - X+1], we have to search for all connections of X+1 in its own monomer as
            # well as [X - X+1 - X+2] cases if the bond is shared

            # check whether we have to check for the first monomer bonds
            if monomer_idx == 0 and self.monomeric_pnum[0].__len__() == 1:
                continue
            elif monomer_idx == 0:
                cbond = min(self.polymer_topology[0])
                for bond in self.bonds:
                    if (bond[1] in self.monomeric_pnum[0]) and (bond[2] in self.monomeric_pnum[0]) \
                            and (bond[1] == cbond or bond[2] == cbond):
                        if bond[1] == cbond:
                            aname = self.type_by_idx(bond[2]) + '-' + self.type_by_idx(cbond) + '-' + \
                                    self.type_by_idx(max(self.polymer_topology[0]))
                            self.angles.append([aname, bond[2], cbond, self.polymer_topology[1]])
                        else:
                            aname = self.type_by_idx(bond[1]) + '-' + self.type_by_idx(cbond) + '-' + \
                                    self.type_by_idx(max(self.polymer_topology[0]))
                            self.angles.append([aname, bond[1], cbond, self.polymer_topology[1]])
                continue
            # cases where the monomer index > 0
            # we have to check for preceding bonding as well as forward and X1 - X2 - X3 bonding

            # preceding bond
            pbond = min(self.polymer_topology[monomer_idx - 1])
            cbond = max(self.polymer_topology[monomer_idx - 1])
            for bond in self.bonds:
                if bond[1] in self.monomeric_pnum[monomer_idx] and bond[2] in self.monomeric_pnum[monomer_idx] \
                        and (bond[1] == cbond or bond[2] == cbond):
                    if bond[1] == cbond:
                        aname = self.type_by_idx(pbond) + '-' + self.type_by_idx(cbond) + '-' + self.type_by_idx(bond[2])
                        self.angles.append([aname, pbond, cbond, bond[2]])
                    else:
                        aname = self.type_by_idx(pbond) + '-' + self.type_by_idx(cbond) + '-' + self.type_by_idx(bond[1])
                        self.angles.append([aname, pbond, cbond, bond[1]])

            # next bond
            cbond = min(self.polymer_topology[monomer_idx])
            nbond = max(self.polymer_topology[monomer_idx])
            for bond in self.bonds:
                if bond[1] in self.monomeric_pnum[monomer_idx] and bond[2] in self.monomeric_pnum[monomer_idx] \
                        and (bond[1] == cbond or bond[2] == cbond):
                    if bond[1] == cbond:
                        aname = self.type_by_idx(bond[2]) + '-' + self.type_by_idx(cbond) + '-' + self.type_by_idx(nbond)
                        self.angles.append([aname, bond[2], cbond, nbond])
                    else:
                        aname = self.type_by_idx(bond[1]) + '-' + self.type_by_idx(cbond) + '-' + self.type_by_idx(
                            nbond)
                        self.angles.append([aname, bond[1], cbond, nbond])

            # check for regular backbone (X1 - X2 - X3 type bonds)
            if max(self.polymer_topology[monomer_idx - 1]) == min(self.polymer_topology[monomer_idx]):
                idx1 = min(self.polymer_topology[monomer_idx - 1])
                idx2 = max(self.polymer_topology[monomer_idx - 1])
                idx3 = max(self.polymer_topology[monomer_idx])
                aname = self.type_by_idx(idx1) + '-' + self.type_by_idx(idx2) + '-' + self.type_by_idx(idx3)
                self.angles.append([aname, idx1, idx2, idx3])


class PolystyreneSulfonateMonomer(LinearChain):
    def __init__(self, tact):
        _units = SimUnits()
        _units.set_length('nm')
        _units.set_mass('amu')
        _units.set_energy('kJ/mol')
        super(PolystyreneSulfonateMonomer, self).__init__(n_monomer=3, kuhn_length=1.0, units=_units)
        self.beads.append(CoarsegrainedBead.bead(beadtype='PSSA', mass=27.0, charge=0.0, position=[0.0, 0.0, 0.0]))
        self.beads.append(CoarsegrainedBead.bead(beadtype='PSSB', mass=72.0, charge=-0.505, position=[1.0, 0.0, 0.0]))
        self.beads.append(CoarsegrainedBead.bead(beadtype='PSSC', mass=80.0, charge=-0.495, position=[2.0, 0.0, 0.0]))
        self.chain_side = tact
        self.bonds = [['PSSAB', 0, 1], ['PSSBC', 1, 2]]
        self.rem_sites = [1, 1]
        self.pnum = [0, 1, 2]
        self.positions = np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [2.0, 0.0, 0.0]], dtype=np.float32)


class PolystyreneSulfonateChain(RandomPolymer):

    def __init__(self, n_mono, tacticity=None):
        _units = SimUnits()
        _units.set_length('nm')
        _units.set_mass('amu')
        _units.set_energy('kJ/mol')

        # create the monomers
        monomers = [PolystyreneSulfonateMonomer(1.0), PolystyreneSulfonateMonomer(-1.0)]
        if tacticity is None or tacticity == 'a':  # assume purely random conformation
            def _mfunc(*args, **kwargs):
                if np.random.uniform() > 0.5:
                    return 0
                else:
                    return 1
        elif tacticity == 'iso' or tacticity == 1:
            def _mfunc(*args, **kwargs):
                return 0
        elif tacticity == 'syndio':
            def _mfunc(arg, *args, **kwargs):
                if arg[-1] == 0:
                    return 1
                else:
                    return 0
        elif hasattr(tacticity, '__call__'):
            _mfunc = tacticity
        else:
            raise SyntaxError(' Cannot parse tacticity of the chain')

        btypematrix = [['PSSAB', 'PSSAB'], ['PSSAB', 'PSSAB']]
        btyper0 = np.ones((2, 2), dtype=np.float32)

        super(PolystyreneSulfonateChain, self).__init__(monomers, _mfunc, n_mono, btypematrix, btyper0, units=_units)

        # set the bonds between A-B to be PSS-iso if the monomers have same tacticity and PSS-syn if tacticity differs
        _junction_idx = 0
        for bondidx in range(0, self.bonds.__len__() - 2):
            if self.bonds[bondidx][0] == 'PSSAB' and self.bonds[bondidx + 1][0] == 'PSSAB':
                if self.tidx_list[_junction_idx] == self.tidx_list[_junction_idx+1]:
                    self.bonds[bondidx][0] = 'PSSAB-iso'
                    self.bonds[bondidx+1][0] = 'PSSAB-iso'
                    _junction_idx += 1
                else:
                    self.bonds[bondidx][0] = 'PSSAB-syn'
                    self.bonds[bondidx+1][0] = 'PSSAB-syn'
                    _junction_idx += 1

        # create angle potentials ABA, BAB (tacticity dependent) / ABC

        for bead_idx in range(0, self.beads.__len__()):
            tidx_pos = bead_idx / 3

            if self.beads[bead_idx].beadtype == 'PSSB':
                nextA = Util.find_next(self.beads, bead_idx, 'PSSA', 'beadtype')
                nextB = Util.find_next(self.beads, bead_idx, 'PSSB', 'beadtype')
                if self.tidx_list.__len__() > tidx_pos + 1 and self.tidx_list[tidx_pos] == self.tidx_list[tidx_pos+1]:
                    if nextA is not None and nextB is not None:
                        self.angles.append(['BAB-iso', bead_idx, nextA, nextB])
                else:
                    if nextA is not None and nextB is not None:
                        self.angles.append(['BAB-syn', bead_idx, nextA, nextB])

                nextC = Util.find_next(self.beads, bead_idx, 'C', 'beadtype')
                if nextA is not None and nextC is not None:
                    self.angles.append(['ABC', nextC, bead_idx, nextA])

            if self.beads[bead_idx].beadtype == 'PSSA':
                nextB = Util.find_next(self.beads, bead_idx, 'PSSB', 'beadtype')
                nextC = Util.find_next(self.beads, bead_idx, 'PSSC', 'beadtype')
                if nextB is not None and nextC is not None:
                    self.angles.append(['ABC', bead_idx, nextB, nextC])

                nextA = Util.find_next(self.beads, bead_idx, 'PSSA')
                if self.tidx_list.__len__() > tidx_pos + 1 and self.tidx_list[tidx_pos] == self.tidx_list[tidx_pos+1]:
                    if nextA is not None and nextB is not None:
                        self.angles.append(['ABA-iso', bead_idx, nextB, nextA])
                else:
                    if nextA is not None and nextB is not None:
                        self.angles.append(['ABA-syn', bead_idx, nextB, nextA])
        # create dihedral angles potentials

            if self.beads[bead_idx].beadtype == 'PSSB':
                nextA = Util.find_next(self.beads, bead_idx, 'PSSA', 'beadtype')
                nextB = Util.find_next(self.beads, bead_idx, 'PSSB', 'beadtype')
                nnextC = Util.find_next(self.beads, nextB, 'PSSC', 'beadtype')
                if nextA is not None and nextB is not None and nnextC is not None:
                    if self.tidx_list[tidx_pos] == self.tidx_list[tidx_pos+1]:
                        self.dihedrals.append(['BABC-iso', bead_idx, nextA, nextB, nnextC])
                    else:
                        self.dihedrals.append(['BABC-syn', bead_idx, nextA, nextB, nnextC])
            if self.beads[bead_idx].beadtype == 'PSSA':
                nextB = Util.find_next(self.beads, bead_idx, 'PSSB', 'beadtype')
                nnextA = Util.find_next(self.beads, nextB, 'PSSA', 'beadtype')
                nnextB = Util.find_next(self.beads, nnextA, 'PSSB', 'beadtype')

                if nextB is not None and nnextA is not None and nnextB is not None:
                    if self.tidx_list[tidx_pos] == self.tidx_list[tidx_pos+1]:
                        self.dihedrals.append(['ABAB-iso', bead_idx, nextB, nnextA, nnextB])
                    else:
                        self.dihedrals.append(['ABAB-syn', bead_idx, nextB, nnextA, nnextB])


class DNAChain(LinearChain):
    def __init__(self, n_ss, n_ds, sticky_end, flexor=None, **kwargs):
        LinearChain.__init__(self, n_monomer=n_ss + n_ds + len(sticky_end), kuhn_length=0.84)
        self.num_ssDNA_bases = n_ss
        self.num_dsDNA_bases = n_ds
        self.sticky_end = sticky_end
        """ Bead types:
        S: ssDNA
        A: dsDNA
        B: sticky-end
        NP: nanoparticle
        Fl: flanking bead
        C: 
        """
        # Bond types
        self.b_types = [['S-NP',     330.0, 0.84],
                        ['S-S',      330.0, 0.84 * 0.5],
                        ['S-A',      330.0, 0.84 * 0.75],
                        ['backbone', 330.0, 0.84],
                        ['A-B',      330.0, 0.84 * 0.75],
                        ['B-B',      330.0, 0.84 * 0.5],
                        ['C-FL',     330.0, 0.84 * 0.5],
                        ['B-FL',     330.0, 0.84 * 0.5 * 1.41],
                        ['C-C',      330.0, 0.84 * 0.5]]
        # Angle types
        self.a_types = [['flexor',     2.0,       pi],
                        ['dsDNA',     30.0,       pi],
                        ['C-C-C',     10.0,       pi],
                        ['B-B-B',     10.0,       pi],
                        ['FL-C-FL',  100.0,       pi],
                        ['A-B-C',    120.0, 0.5 * pi],
                        ['A-A-B',      2.0,       pi],
                        ['A-B-B',      2.0,       pi],
                        ['C-C-endFL', 50.0,       pi]]
        self.rem_sites = []  # we dont attach anything to the DNA chain
        self.inner_types = []
        if flexor is not None:
            self.flexors = flexor - self.num_ssDNA_bases
        else:
            self.flexors = flexor
        if self.num_dsDNA_bases < 2:
            self.flexors = []

        if self.nmono < 1:
            raise StandardError('DNA chain does not have a single monomer, unable to build')

        self.__build_chain()
        self.__build_bonds()
        self.__build_angles()
        self.__build_beads()

    #############################
    # to be deprecated
    # overloading older methods
    @property
    def harmonic_bonds_in_one_dna_chain(self):
        return self.bonds

    @property
    def angles_in_one_dna_chain(self):
        return self.angles

    @property
    def beads_in_one_dna_chain(self):
        return self.beads
    ########################

    def __build_chain(self):
        num_ss_left = self.num_ssDNA_bases
        num_ds_left = self.num_dsDNA_bases
        num_stick_left = len(self.sticky_end)
        monomers_left = self.nmono
        sticky_left = copy.deepcopy(self.sticky_end)

        # Build the chain one bead at a time, in the order of
        #   1) ssDNA
        #   2) dsDNA
        #   3) sticky-end
        while monomers_left > 0:
            # Add ssDNA
            if num_ss_left > 0:
                num_ss_left -= 1
                self.inner_types.append('S')
                self.mass.append(1.0)
                try:
                    self.pnum.append(self.pnum[-1] + 1)
                except IndexError:
                    self.pnum.append(0)

            # Add dsDNA
            elif num_ds_left > 0:
                num_ds_left -= 1
                self.inner_types.append('A')
                self.mass.append(4.0)
                try:
                    self.pnum.append(self.pnum[-1] + 1)
                except IndexError:
                    print 'Warning : DNA chain starts with dsDNA'
                    self.pnum.append(0)

            # Add the sticky end
            else:
                num_stick_left -= 1
                self.inner_types.append('B')
                self.mass.append(1.0)
                try:
                    self.pnum.append(self.pnum[-1] + 1)
                except IndexError:
                    print 'Warning : DNA chain starts with sticky ends, there is no linker / duplexor, check for errors'
                    self.pnum.append(0)
                self.inner_types.append(sticky_left.pop(0))
                self.mass.append(1.0)
                self.pnum.append(self.pnum[-1] + 1)
                # Add flanker beads to make sure the chain doesn't bind to multiple other chains
                self.inner_types.append('FL')
                self.mass.append(1.0)
                self.pnum.append(self.pnum[-1] + 1)
                self.inner_types.append('FL')
                self.mass.append(1.0)
                self.pnum.append(self.pnum[-1] + 1)
            monomers_left -= 1

        # If there is a sticky end, the ends of the sticky ends could also bind
        # Add another flanking bead to protect against this
        if len(self.sticky_end) > 1:
            self.inner_types.append('FL')
            self.mass.append(1.0)
            self.pnum.append(self.pnum[-1] + 1)

    def __build_bonds(self):
        for idx in range(len(self.pnum) - 1):
            # ssDNA - ssDNA
            if self.inner_types[idx] == 'S' and self.inner_types[idx + 1] == 'S':
                self.bonds.append(['S-S', idx, idx + 1])
            # ssDNA - dsDNA
            if self.inner_types[idx] == 'S' and self.inner_types[idx + 1] == 'A':
                self.bonds.append(['S-A', idx, idx + 1])
            # dsDNA - dsDNA
            if self.inner_types[idx] == 'A' and self.inner_types[idx + 1] == 'A':
                self.bonds.append(['backbone', idx, idx + 1])
            # dsDNA - sticky-end
            if self.inner_types[idx] == 'A' and self.inner_types[idx + 1] == 'B':
                self.bonds.append(['A-B', idx, idx + 1])
            # ssDNA - sticky-end
            if self.inner_types[idx] == 'S' and self.inner_types[idx + 1] == 'B':  # special case with no dsDNA
                self.bonds.append(['S-B', idx, idx + 1])

            ###
            # Stuff get messier afterwards. for each B, there is 1 sticky + 2 flanking
            # plus the end flanking to check.
            ###
            # sticky-end - sticky-end
            if self.inner_types[idx] == 'B' and self.inner_types[idx + 4] == 'B':
                self.bonds.append(['B-B', idx, idx + 4])
            # sticky-end - flanker1
            if self.inner_types[idx] == 'B' and self.inner_types[idx + 2] == 'FL':
                self.bonds.append(['B-FL', idx, idx + 2])
            # sticky-end - flanker2
            if self.inner_types[idx] == 'B' and self.inner_types[idx + 3] == 'FL':
                self.bonds.append(['B-FL', idx, idx + 3])
            # sticky-end - ?
            # TODO: what is C?
            if self.inner_types[idx] == 'B' and self.inner_types[idx + 1] in self.sticky_end:
                self.bonds.append(['B-C', idx, idx + 1])
            if self.inner_types[idx] == 'B' and self.inner_types[idx + 4] == 'FL':
                self.bonds.append(['B-FL', idx, idx + 4])

            if self.inner_types[idx] in self.sticky_end and self.inner_types[idx + 1] == 'FL':
                self.bonds.append(['C-FL', idx, idx + 1])
            if self.inner_types[idx] in self.sticky_end and self.inner_types[idx + 2] == 'FL':
                self.bonds.append(['C-FL', idx, idx + 2])

            try:
                if self.inner_types[idx] in self.sticky_end and self.inner_types[idx + 4] in self.sticky_end:
                    self.bonds.append(['C-C', idx, idx + 4])
            except IndexError:
                pass
            if self.inner_types[idx] in self.sticky_end and self.inner_types[idx + 3] == 'FL':
                self.bonds.append(['C-FL', idx, idx + 3])

    def __build_angles(self):
        for idx in range(len(self.pnum) - 2):
            if self.inner_types[idx] == 'S':
                continue
            if self.inner_types[idx] == 'A':
                if self.inner_types[idx + 1] == 'A' and self.inner_types[idx + 2] == 'A':
                    if self.flexors is not None and idx + 1 in self.flexors:
                        self.angles.append(['flexor', idx, idx + 1, idx + 2])
                    else:
                        self.angles.append(['dsDNA', idx, idx + 1, idx + 2])
                if self.inner_types[idx + 1] == 'A' and self.inner_types[idx + 2] == 'B':
                    self.angles.append(['A-A-B', idx, idx + 1, idx + 2])
                try:
                    # offset by 4 see generator above
                    if self.inner_types[idx + 1] == 'B' and self.inner_types[idx + 5] == 'B':
                        self.angles.append(['A-B-B', idx, idx + 1, idx + 5])
                except IndexError:
                    pass
                try:
                    if self.inner_types[idx + 1] == 'B' and self.inner_types[idx + 2] in self.sticky_end:
                        self.angles.append(['A-B-C', idx, idx + 1, idx + 2])
                except IndexError:
                    pass
            try:
                if self.inner_types[idx] == 'B' \
                        and self.inner_types[idx + 4] == 'B'\
                        and self.inner_types[idx + 8] == 'B':
                    self.angles.append(['B-B-B', idx, idx + 4, idx + 8])
            except IndexError:
                pass
            # have to check angles FL - C - FL, C - C - C and C - C - FL
            if self.inner_types[idx] in self.sticky_end:
                # should be, sanity check
                if self.inner_types[idx + 1] == 'FL' and self.inner_types[idx + 2] == 'FL':
                    self.angles.append(['FL-C-FL', idx + 1, idx, idx + 2])
                try:
                    if self.inner_types[idx + 4] in self.sticky_end and self.inner_types[idx + 8] in self.sticky_end:
                        self.angles.append(['C-C-C', idx, idx + 4, idx + 8])
                except IndexError:
                    pass
                try:
                    if self.inner_types[idx + 4] in self.sticky_end and self.inner_types[idx + 7] == 'FL':
                        self.angles.append(['C-C-endFL', idx, idx+4, idx+7])
                except IndexError:
                    pass

    def __build_beads(self):

        base_length = self.lmono

        for idx in range(len(self.pnum)):
            if idx == 0:
                self.positions = np.append(self.positions, [[0.0, 0.0, 0.0]], axis=0)
                continue
            # S - S bond
            if self.inner_types[idx - 1] == 'S' and self.inner_types[idx] == 'S':
                self.positions = np.append(self.positions,
                                           self.positions[-1, :] + [[0.0, 0.0, 0.50 * base_length]],
                                           axis=0)
            # S - A bond
            elif self.inner_types[idx - 1] == 'S' and self.inner_types[idx] == 'A':
                self.positions = np.append(self.positions,
                                           self.positions[-1, :] + [[0.0, 0.0, 0.50 * base_length]],
                                           axis=0)
            # A - A bond
            elif self.inner_types[idx - 1] == 'A' and self.inner_types[idx] == 'A':
                self.positions = np.append(self.positions,
                                           self.positions[-1, :] + [[0.0, 0.0, 1.00 * base_length]],
                                           axis=0)
            elif self.inner_types[idx] == 'B':
                if self.inner_types[idx - 1] == 'A':
                    self.positions = np.append(self.positions,
                                               self.positions[-1, :] + [[0.0, 0.0, 0.50 * base_length]],
                                               axis=0)
                # special case where sticky ends are bound to ssDNA
                elif self.inner_types[idx - 1] == 'S':
                    self.positions = np.append(self.positions,
                                               self.positions[-1, :] + [[0.0, 0.0, 0.25 * base_length]],
                                               axis=0)
                # sanity check
                elif self.inner_types[idx - 4] == 'B':
                    self.positions = np.append(self.positions,
                                               self.positions[-4, :] + [[0.0, 0.0, 0.50 * base_length]],
                                               axis=0)
            # 3 scenarios, 1st flanking for C, 2nd flanking for C or chain end FL
            elif self.inner_types[idx] == 'FL':
                if self.inner_types[idx - 2] == 'B':
                    self.positions = np.append(self.positions,
                                               self.positions[-2, :] + [[0.30 * base_length, 0.40 * base_length, 0.0]],
                                               axis=0)
                elif self.inner_types[idx - 3] == 'B':
                    self.positions = np.append(self.positions,
                                               self.positions[-3, :] + [[-0.30 * base_length, 0.40 * base_length, 0.0]],
                                               axis=0)
                # chain end flanking
                elif self.inner_types[idx - 4] == 'B':
                    self.positions = np.append(self.positions,
                                               self.positions[-4, :] + [[0.0, 0.0, 0.30 * base_length]],
                                               axis=0)
            elif self.inner_types[idx] in self.sticky_end:
                self.positions = np.append(self.positions,
                                           self.positions[-1, :] + [[0.0, 0.40 * base_length, 0.0]],
                                           axis=0)

        for idx in range(len(self.pnum)):
            self.beads.append(CoarsegrainedBead.bead(position=self.positions[idx, :],
                                                     beadtype=self.inner_types[idx],
                                                     mass=self.mass[idx],
                                                     body=-1))


X3DNA_LICENSE = """
3DNA is a suite of software programs for the analysis,
rebuilding and visualization of 3-Dimensional Nucleic Acid
structures. Permission to use, copy, modify, and distribute
this suite for any purpose, with or without fee, is hereby
granted, and subject to the following conditions:
At least one of the 3DNA papers must be cited, including the
following two primary ones:

   1. Lu, X. J., & Olson, W. K. (2003). "3DNA: a software
      package for the analysis, rebuilding and visualization
      of three-dimensional nucleic acid structures." Nucleic
      Acids Research, 31(17), 5108-5121.

   2. Lu, X. J., & Olson, W. K. (2008). "3DNA: a versatile,
      integrated software system for the analysis,
      rebuilding and visualization of three-dimensional
      nucleic-acid structures." Nature Protocols, 3(7),
      1213-1227.

THE 3DNA SOFTWARE IS PROVIDED "AS IS", WITHOUT EXPRESSED OR
IMPLIED WARRANTY OF ANY KIND.

Any 3DNA-related questions, comments, and suggestions are
welcome and should be directed to the open 3DNA Forum
(http://forum.x3dna.org/).
"""
DNA3SPN_LICENSE = """
### Oct. 12, 2015 ###
The GCGI/ directory was added, which contains necessary files for using
the general coarse- grained ions model presented by Hinckley and de Pablo,
JCTC (2015) (DOI:10.1021/acs.jctc.5b00341).

The following website will have information regarding on-going improvements to
the 3SPN.2 model, as well as recent publications by the de Pablo group and
others.

http://ime.uchicago.edu/de_pablo_lab/research/dna_folding_and_hybridization/3spn.2/
"""


class Dna3spnChain(LinearChain):  # TODO : add access to protected var by using a staticmethod yielding the values
    _LICENSE_DISPLAY = True
    _MOL_INDEX = 1  # this has chain index that need to be incremented for each molecule. Check do_on_copy() for usage
    _MASS_TABLE_AMU = {'Phos':  94.97,
                       'Sug':   83.11,
                       'Ade':  134.10,
                       'Thy':  125.10,
                       'Cyt':  110.10,
                       'Gua':  150.10}
    _ALL_TYPES = ['Phos', 'Sug', 'Ade', 'Thy', 'Gua', 'Cyt']
    _BASE_TYPES = ['Ade', 'Thy', 'Gua', 'Cyt']
    _EXCL_DIAM_LIST = {'Phos': 4.5,
                       'Sug':  6.2,
                       'Ade':  5.4,
                       'Thy':  7.1,
                       'Gua':  4.9,
                       'Cyt':  6.4}
    try:
        X3DNA_REL_PATH = os.path.abspath(os.environ['PBS_O_WORKDIR'])
    except KeyError:
        X3DNA_REL_PATH = os.path.abspath('./')
        print 'Could not find PBS_O_WORKDIR variable, setting X3 DNA path to ' + X3DNA_REL_PATH + '/DNA3SPN/X3DNA'
        print ('X3DNA is only required for the DNA 3SPN chain model '
               'and path can manually be set using LinearChain.Dna3spnChain.X3DNA_REL_PATH')

    def __init__(self, ss_dna_sequence=None, ds_dna_sequence=None, sticky_sequence=None, flexor=None, mpi_rank=None,
                 mpi_sync_method=None, **kwargs):
        # Set the units
        _units = SimUnits()
        _units.set_energy('kJ/mol')
        _units.set_length('Ang')
        _units.set_mass('amu')

        # Initialize the data structure for a linear chain
        LinearChain.__init__(self,
                             n_monomer=(len(ss_dna_sequence) + len(ds_dna_sequence) + len(sticky_sequence)) * 3,
                             kuhn_length=0.00,
                             units=_units)

        # Check that the files necessary to build a DNA chain are present
        if not os.path.isfile(Dna3spnChain.X3DNA_REL_PATH + '/DNA3SPN/X3DNA/bin/x3dna_utils'):
            print 'X3 DNA package is required to build 3SPN DNA model'
            raise IOError('Could not find X3 DNA package at '
                          + Dna3spnChain.X3DNA_REL_PATH + '/DNA3SPN/X3DNA/bin/x3dna_utils')

        if not os.access(Dna3spnChain.X3DNA_REL_PATH + '/DNA3SPN/X3DNA/bin/x3dna_utils', os.X_OK):
            print 'Packages requires execute permission on /bin/x3dna_utils of X3DNA'
            raise IOError('x3dna_utils reported not having os.X_OK bit set')

        if not os.access(Dna3spnChain.X3DNA_REL_PATH + '/DNA3SPN/X3DNA/bin/rebuild', os.X_OK):
            print 'Packages requires execute permissions on /bin/rebuild of X3DNA'
            raise IOError('rebuild reported not having os.X_OK bit set')

        # Display the license information the first time the code is run
        if Dna3spnChain._LICENSE_DISPLAY:
            print 'This model uses X3 DNA to build the representation'
            print X3DNA_LICENSE

            print 'This model makes use of parts of the USER-3SPN2 package'
            print DNA3SPN_LICENSE

            print 'License information is displayed only once'
            Dna3spnChain._LICENSE_DISPLAY = False

        # check that sequences are composed of the characters ATCG
        try:
            for b in ss_dna_sequence:
                if b not in ['A', 'T', 'C', 'G']:
                    raise SyntaxError('Sequences of bases require iterables of A, T, C, G')
        except TypeError:
            raise TypeError('squences must be of iterable types')

        try:
            for b in ds_dna_sequence:
                if b not in ['A', 'T', 'C', 'G']:
                    raise SyntaxError('Sequences of bases require iterables of A, T, C, G')
        except TypeError:
            raise TypeError('squences must be of iterable types')

        try:
            for b in sticky_sequence:
                if b not in ['A', 'T', 'C', 'G']:
                    raise SyntaxError('Sequences of bases require iterables of A, T, C, G')
        except TypeError:
            raise TypeError('squences must be of iterable types')

        if flexor is not None:
            raise NotImplementedError('flexors will be implemented in a future version')

        # Copy the sequences
        self.ss_sequence = [nucleotide for nucleotide in ss_dna_sequence]
        self.ds_sequence = [nucleotide for nucleotide in ds_dna_sequence]
        self.sticky_sequence = [self.get_complement(nucleotide) for nucleotide in sticky_sequence]

        self.three_prime_end = 0
        self.five_prime_end = 0

        # If this is a serial simulation or the first rank of an MPI simulation,
        # create an atomistic structure of the DNA
        if mpi_rank is None:
            self.write_dna_structure()
        elif mpi_rank == 0:
            self.write_dna_structure()
        if mpi_sync_method is not None:
            mpi_sync_method()

        # Use 3SPN to coarse-grain the atomistic structure and generate a structure and topology
        self.subsystem = DNA3SPN.pdb2cg_dna.System()
        DNA3SPN.pdb2cg_dna.read_pdb(os.path.join(os.path.abspath('./'), '/atomistic.pdb'),
                                    self.subsystem)
        self.subsystem.do_coarse_graining()
        self.subsystem.generate_topology()

        # Convert this 3SPN topology to a hoobas topology
        self.generate_from_3spn_topologies()

        # the hoomd code swaps A-T and C-G parameters depending on quat.x
        self.set_quaternion_inversion()
        # set charges
        self.set_charge_phosphates()
        # move the chain upwards in Z
        self.set_position_array()
        self.generate_3base_bonds()
        # the 3' end is bound to the NP and not the 5' end, we need to reverse the chain order, this is just semantics
        # self.swap_ends()
        self.zero_pos = np.array([0.0, 0.0, 5.0])

        self.strict_topology = True
        self.set_masses()

    def write_dna_structure(self):
        # Write the configuration file
        DNA3SPN.make_bp_params.write_bp_parameters(self.ss_sequence + self.ds_sequence + self.sticky_sequence)
        # Modify the parameters to use B-DNA
        # Make sure the X3DNA environmental variable is set so it can find itself
        try:
            os.environ['X3DNA']
        except KeyError:
            os.environ['X3DNA'] = os.path.join(Dna3spnChain.X3DNA_REL_PATH, 'DNA3SPN/X3DNA/')
        parameter_modifier = os.path.join(Dna3spnChain.X3DNA_REL_PATH, 'DNA3SPN/X3DNA/bin/x3dna_utils')
        use_b_dna_command = '{} {} {}'.format(parameter_modifier, 'cp_std', 'BDNA')
        os.system(use_b_dna_command)
        # Build the DNA structure and write it a file
        dna_builder = os.path.join(Dna3spnChain.X3DNA_REL_PATH, 'DNA3SPN/X3DNA/bin/rebuild')
        parameter_path = os.path.join(os.path.abspath('.'), 'bp_step.par')
        dna_structure_path = os.path.join(os.path.abspath('.'), 'atomistic.pdb')
        build_dna_command = '{} -atomic {} {}'.format(dna_builder, parameter_path, dna_structure_path)
        os.system(build_dna_command)
        return {'dna_parameter_path': parameter_path,
                'dna_structure_path': dna_structure_path}

    def generate_from_3spn_topologies(self):
        # convert subsystem topology to local topology
        # note that subsystem topology always run 5' to 3', with beads Sugar - Base - Phosphate - Sugar - ....
        # The system we want looks something like this:
        #                 ssDNA    |          dsDNA             |  sticky-end
        # Chain 5:  5' ACTGACTGACTG ACTGACTGACTGACTGACTGACTGACTG                  3'
        # Chain 3:  3'              TGACTGACTGACTGACTGACTGACTGAC ACTGACTGACTGACTG 5'
        #
        # However, it is easiest to initialize the strands as only double-stranded, ie
        # Chain 5:  5' ACTGACTGACTG ACTGACTGACTGACTGACTGACTGACTG ACTGACTGACTGACTG 3'
        # Chain 3:  3' TGACTGACTGAC TGACTGACTGACTGACTGACTGACTGAC TGACTGACTGACTGAC 5'
        #
        # We need to excise the unwanted parts of the DNA
        beadtype_dict = {'S': 'Sug',
                         'P': 'Phos',
                         'A': 'Ade',
                         'T': 'Thy',
                         'C': 'Cyt',
                         'G': 'Gua'}

        # Each particle has an index associated with it.
        # track the indices for the 5' --> 3' chain, and 3' --> 5' chain separately
        # These two indices contain the LAMMPS indices, not the the hoobas indices
        chain5to3_indices = []
        chain3to5_indices = []
        chain5to3 = self.subsystem.molecules[0]
        chain3to5 = self.subsystem.molecules[1]
        num_bases_chain5to3 = len(self.ss_sequence) + len(self.ds_sequence)
        num_bases_chain3to5 = len(self.sticky_sequence) + len(self.ds_sequence)
        chain3to5_offset = len(self.subsystem.molecules[0].beads)

        # Chain 5' --> 3'
        # Copy the beads from the cg model, excluding the sticky end
        # TODO: why is this check is necessary?
        first_sticky_bead_idx = min(3 * num_bases_chain5to3,  # End of ss-DNA / ds-DNA
                                    len(chain5to3.beads))  # End of chain
        for local_bead in chain5to3.beads[0:first_sticky_bead_idx]:
            # The chain_index represents the chain for which a base belongs
            # If the bead is a dna base, it is +1 for the 5' --> 3' chain, and -1 for the 3' --> 5' chain
            # else: it is 0 (not a dna base)
            # If the dna chain is copied, these numbers will be incremented by +1 and -1 for the 5' --> 3'
            # and 3' --> 5' chains respectively
            if beadtype_dict[local_bead.beadtype] in ['Ade', 'Thy', 'Cyt', 'Gua']:
                chain_index = 1.0
            else:
                chain_index = 0.0
            self.beads.append(CoarsegrainedBead.bead(position=[local_bead.x, local_bead.y, local_bead.z],
                                                     charge=local_bead.charge,
                                                     beadtype=beadtype_dict[local_bead.beadtype],
                                                     diameter=chain_index))
            chain5to3_indices.append(local_bead.beadnumber)

        num_beads_chain5to3 = len(self.beads)
        self.three_prime_end = len(self.beads) - 1

        # Chain 3' --> 5'
        # Copy the second strand of DNA, excluding the ssDNA and including the sticky end
        # The chain starts at the 5' end, so the sticky end bp come first, then the dsDNA
        # this cuts the unused section of the 5' - 3' sense strand (removes the ssDNA sequence)
        # Exclude the last bead of the 3' --> 5' strand
        for idx in range(3 * len(self.sticky_sequence) + 3 * len(self.ds_sequence) - 1):
            lb = self.subsystem.molecules[1].beads
            if beadtype_dict[lb[idx].beadtype] in ['Ade', 'Thy', 'Cyt', 'Gua']:
                chain_index = -1.0
            else:
                chain_index = 0.0
            self.beads.append(CoarsegrainedBead.bead(position=[lb[idx].x, lb[idx].y, lb[idx].z],
                                                     charge=lb[idx].charge,
                                                     beadtype=beadtype_dict[lb[idx].beadtype],
                                                     diameter=chain_index))
            chain3to5_indices.append(lb[idx].beadnumber + chain3to5_offset)  # who the hell offset half his lists

        # Set the ID for each bead
        self.pnum = [particle_id for particle_id in range(len(self.beads))]

        # Translate the bonds from lammps
        # Chain 5' --> 3'
        for local_bond in chain5to3.bonds:
            # Add the bond if
            #   1) we kept both beads
            #   2) the type is not -1
            if local_bond.stea in chain5to3_indices and local_bond.steb in chain5to3_indices and local_bond.type != -1:
                bead1_idx = local_bond.stea - 1
                bead2_idx = local_bond.steb - 1
                bond_name = "{}-{}".format(self.beads[bead1_idx].beadtype, self.beads[bead2_idx].beadtype)
                new_bond = ([bond_name, bead1_idx, bead2_idx])
                self.bonds.append(new_bond)
                # Each bond includes the name, and the atoms involved in the bond
                self.b_types.append([bond_name,
                                     60.0,
                                     np.linalg.norm(self.beads[bead1_idx].position - self.beads[bead2_idx].position)])
        # Chain 3' --> 5'
        for local_bond in chain3to5.bonds:
            if local_bond.stea in chain3to5_indices and local_bond.steb in chain3to5_indices and local_bond.type != -1:
                bead1_idx = chain3to5_indices.index(local_bond.stea) + num_beads_chain5to3
                bead2_idx = chain3to5_indices.index(local_bond.steb) + num_beads_chain5to3
                bond_name = "{}-{}".format(self.beads[bead1_idx].beadtype, self.beads[bead2_idx].beadtype)
                new_bond = ([bond_name, bead1_idx, bead2_idx])
                self.bonds.append(new_bond)
                self.b_types.append([bond_name,
                                     60.0,
                                     np.linalg.norm(self.beads[bead1_idx].position - self.beads[bead2_idx].position)])

        # Angles
        # Chain 5' --> 3'
        for local_angle in chain5to3.bends:
            if local_angle.stea in chain5to3_indices and local_angle.steb in chain5to3_indices \
                    and local_angle.stec in chain5to3_indices and local_angle.type != -1:
                bead1_idx = local_angle.stea - 1
                bead2_idx = local_angle.steb - 1
                bead3_idx = local_angle.steb - 1
                angle_name = '{}-{}-{}'.format(self.beads[bead1_idx].beadtype,
                                               self.beads[bead2_idx].beadtype,
                                               self.beads[bead3_idx].beadtype)
                self.angles.append([angle_name, bead1_idx, bead2_idx, bead3_idx])

        # Chain 3' --> 5'
        for local_angle in chain3to5.bends:
            if local_angle.stea in chain3to5_indices and local_angle.steb in chain3to5_indices\
                    and local_angle.stec in chain3to5_indices and local_angle.type != -1:
                bead1_idx = chain3to5_indices.index(local_angle.stea) + num_beads_chain5to3
                bead2_idx = chain3to5_indices.index(local_angle.steb) + num_beads_chain5to3
                bead3_idx = chain3to5_indices.index(local_angle.stec) + num_beads_chain5to3
                angle_name = '{}-{}-{}'.format(self.beads[bead1_idx].beadtype,
                                               self.beads[bead2_idx].beadtype,
                                               self.beads[bead3_idx].beadtype)
                self.angles.append([angle_name, bead1_idx, bead2_idx, bead3_idx])

        # Dihedrals
        # Chain 5' --> 3'
        for local_diehedral in chain5to3.torsions:
            if local_diehedral.stea in chain5to3_indices and \
                            local_diehedral.steb in chain5to3_indices and \
                            local_diehedral.stec in chain5to3_indices and \
                            local_diehedral.sted in chain5to3_indices and \
                            local_diehedral.type != -1:
                bead1_idx = local_diehedral.stea - 1
                bead2_idx = local_diehedral.steb - 1
                bead3_idx = local_diehedral.stec - 1
                bead4_idx = local_diehedral.sted - 1
                dihedral_name = '{}-{}-{}-{}'.format(self.beads[bead1_idx], self.beads[bead2_idx],
                                                     self.beads[bead3_idx], self.beads[bead4_idx])
                self.dihedrals.append([dihedral_name,
                                       bead1_idx, bead2_idx, bead3_idx, bead4_idx])
        # Chain 3' --> 5'
        for local_diehedral in chain3to5.torsions:
            if local_diehedral.stea in chain3to5_indices and \
                            local_diehedral.steb in chain3to5_indices and \
                            local_diehedral.stec in chain3to5_indices and \
                            local_diehedral.sted in chain3to5_indices and \
                            local_diehedral.type != -1:
                bead1_idx = chain3to5_indices.index(local_diehedral.stea) + num_beads_chain5to3
                bead2_idx = chain3to5_indices.index(local_diehedral.steb) + num_beads_chain5to3
                bead3_idx = chain3to5_indices.index(local_diehedral.stec) + num_beads_chain5to3
                bead4_idx = chain3to5_indices.index(local_diehedral.sted) + num_beads_chain5to3
                dihedral_name = '{}-{}-{}-{}'.format(self.beads[bead1_idx], self.beads[bead2_idx],
                                                     self.beads[bead3_idx], self.beads[bead4_idx])
                self.dihedrals.append([dihedral_name,
                                       bead1_idx, bead2_idx, bead3_idx, bead4_idx])

    def generate_base_stacking_angles(self):
        pass

    def generate_3base_bonds(self):
        # Using the diameter attribute to represent the chain each bead belongs to is confusing
        # Create a helper function to make this explicit
        chain = operator.attrgetter('diameter')
        # generate 'dummy' bonds between bases less than 4 away to avoid intra-strand base pairing
        num_beads = len(self.beads)
        for idx, bead in enumerate(self.beads):
            # Check that it is a nucleotide and not a sugar/phosphate
            if str(bead) in ['Ade', 'Thy', 'Cyt', 'Gua']:
                # Check the next three neighbors
                for offset in range(1, 4):
                    next_bead_idx = idx + 3 * offset
                    # Make sure we are still on the same chain
                    if next_bead_idx < num_beads and chain(bead) == chain(self.beads[next_bead_idx]):
                        self.bonds.append(['dummy', idx, next_bead_idx])

    def set_quaternion_inversion(self):
        for bead in self.beads:
            if str(bead) == 'Thy':
                bead.orientation = np.array([-1.0, 0.0, 0.0, 0.0], dtype=np.single)
            if str(bead) == 'Gua':
                bead.orientation = np.array([0.5, 0.0, 0.0, 0.0], dtype=np.single)
            if str(bead) == 'Ade':
                bead.orientation = np.array([1.0, 0.0, 0.0, 0.0], dtype=np.single)
            if str(bead) == 'Cyt':
                bead.orientation = np.array([-0.50, 0.0, 0.0, 0.0], dtype=np.single)

        # Using the diameter attribute to represent the chain each bead belongs to is confusing
        # Create a helper function to make this explicit
        chain = operator.attrgetter('diameter')

        # find terminal bases
        for idx, bead in enumerate(self.beads):
            if str(bead) in Dna3spnChain._BASE_TYPES:
                if 6 <= idx <= (len(self.beads) - 1 - 6):
                    this_chain = chain(bead)
                    if this_chain == chain(self.beads[idx - 6]) and this_chain == chain(self.beads[idx + 6]):
                        bead.orientation += np.array([0.0, 1.0, 1.0, -1.0], dtype=np.single)
                    elif this_chain == chain(self.beads[idx - 6]) and this_chain == chain(self.beads[idx + 3]):
                        bead.orientation += np.array([0.0, -1.0, 1.0, -1.0])
                    elif this_chain == chain(self.beads[idx - 6]) and this_chain != chain(self.beads[idx + 3]):
                        bead.orientation += np.array([0.0, -1.0, 1.0, 1.0])
                    elif this_chain == chain(self.beads[idx + 6]) and this_chain == chain(self.beads[idx - 3]):
                        bead.orientation += np.array([0.0, 1.0, -1.0, -1.0])
                    elif this_chain == chain(self.beads[idx + 6]) and this_chain != chain(self.beads[idx - 3]):
                        bead.orientation += np.array([0.0, 1.0, -1.0, 1.0])
                # First base
                elif 0 <= idx < 3:
                    bead.orientation += np.array([0.0, 1.0, -1.0, 1.0])
                # Second base
                elif 3 <= idx < 6:
                    bead.orientation += np.array([0.0, 1.0, -1.0, -1.0])
                # Last base
                elif (len(self.beads) - 1 - 3) < idx <= (len(self.beads) - 1):
                    bead.orientation += np.array([0.0, -1.0, 1.0, 1.0])
                # Second to last base
                elif (len(self.beads) - 1 - 6) < idx <= (len(self.beads) - 1 - 3):
                    bead.orientation += np.array([0.0, -1.0, 1.0, -1.0])
                else:
                    raise ArithmeticError('Bad number of DNA bases somewhere')

    def set_charge_phosphates(self):
        for bead in self.beads:
            if str(bead) == 'Phos':
                bead.charge = -1.0

    def set_mass_particles(self):
        for bead in self.beads:
            bead.mass = Dna3spnChain._MASS_TABLE_AMU[str(bead)]

    def do_on_copy(self):
        for bead in self.beads:
            bead.diameter *= Dna3spnChain._MOL_INDEX
        Dna3spnChain._MOL_INDEX += 1

    def set_position_array(self):
        self.positions = np.zeros((len(self.beads), 3), dtype=np.single)
        for idx in range(len(self.beads)):
            self.positions[idx, :] = self.beads[idx].position

    @staticmethod
    def force_field_types():
        return Dna3spnChain._ALL_TYPES

    @staticmethod
    def force_field_excluded_volumes():
        return Dna3spnChain._EXCL_DIAM_LIST

    @staticmethod
    def force_field_amus():
        return Dna3spnChain._MASS_TABLE_AMU

    @staticmethod
    def get_excl_sig():
        #                 0       1      2      3      4      5
        # _ALL_TYPES = ['Phos', 'Sug', 'Ade', 'Thy', 'Gua', 'Cyt']
        for ite1 in range(len(Dna3spnChain._ALL_TYPES)):
            for ite2 in range(ite1, len(Dna3spnChain._ALL_TYPES)):
                if ite1 == 2 and ite2 == 3 or ite1 == 4 and ite2 == 5:
                    yield (Dna3spnChain._ALL_TYPES[ite1],
                           Dna3spnChain._ALL_TYPES[ite2],
                           0.0,
                           0.0)
                else:
                    sig1 = Dna3spnChain._EXCL_DIAM_LIST[Dna3spnChain._ALL_TYPES[ite1]]
                    sig2 = Dna3spnChain._EXCL_DIAM_LIST[Dna3spnChain._ALL_TYPES[ite2]]
                    yield (Dna3spnChain._ALL_TYPES[ite1],
                           Dna3spnChain._ALL_TYPES[ite2],
                           (sig1 + sig2) * 0.5,
                           (sig1 + sig2) * 0.5 * 2.0 ** (1.0 / 6.0))

    def randomize_dirs(self, tol=1e-1):  # can't randomize this thing or it'll have insane topology
        pass

    def set_masses(self):
        for bead in self.beads:
            bead.mass = Dna3spnChain._MASS_TABLE_AMU[str(bead)]

    def swap_ends(self):
        """
        swaps ends of the chain to bind to the three prime end instead of 5 prime
        :return:
        """
        self.rotate(direction=[-self.dir[0],
                               -self.dir[1],
                               -self.dir[2]])
        # Get the index of the 3' bead
        old_3p_idx = self.three_prime_end
        new_5p_idx = old_3p_idx
        # Get the index of the 5' bead
        old_5p_idx = self.five_prime_end
        new_3p_idx = old_5p_idx
        # Swap the beads at these locations
        self.beads[old_3p_idx], self.beads[old_5p_idx] = self.beads[old_5p_idx], self.beads[old_3p_idx]
        #
        new_chain_5to3, new_chain_3to5 = (copy.deepcopy(self.positions[old_5p_idx, :]),
                                          copy.deepcopy(self.positions[old_3p_idx, :]))
        self.positions[new_5p_idx, :] = new_chain_5to3
        self.positions[new_3p_idx, :] = new_chain_3to5
        # self.pnum[old_3p_idx], self.pnum[old_5p_idx] = self.pnum[old_5p_idx], self.pnum[old_3p_idx]
        for topo_item in self.bonds + self.angles + self.dihedrals + self.impropers:
            for index in range(1, len(topo_item)):
                if topo_item[index] == old_3p_idx:
                    topo_item[index] = new_3p_idx
                elif topo_item[index] == old_5p_idx:
                    topo_item[index] = new_5p_idx

    @staticmethod
    def get_complement(base):
        base_pairs = {'A': 'T',
                      'T': 'A',
                      'C': 'G',
                      'G': 'C'}
        return base_pairs[base]
