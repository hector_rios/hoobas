#coding: utf-8
import random
from fractions import gcd
from math import *
import warnings

import numpy as np
import copy

from Util import Vector
from Util import get_inverse_rotation_matrix
from Util import uniquetol
from Units import SimulationUnits as SimUnits


class CenterFile(object):
    """
    Provides a class to manage positions of colloids. The class manages types of centers in self.built_centers and
    self.table is a list of list of positions. The first dimension of self.table matches the size of self.built_centers
    """
    def __init__(self, units=None):
        """
        Init method
        :return:
        """
        self.table_size = 0
        self.table = []
        self.BoxSize = 0
        self.built_centers = []
        self.flags = {}

        self.vx = [self.BoxSize, 0.0, 0.0]
        self.vy = [0.0, self.BoxSize, 0.0]
        self.vz = [0.0, 0.0, self.BoxSize]
        self.vmat = np.array([self.vx, self.vy, self.vz])

        if units is not None and issubclass(units.__class__, SimUnits):
            self.units = units
        else:
            self.units = SimUnits()

    # list of usual useful properties, getters and setters
    @property
    def rot_crystal_box(self):
        """
        returns box axes in the new (x,y,z)
        :return:
        """
        return self.vx, self.vy, self.vz

    @property  # DEPRECATED, legacy
    def particle_number(self):
        """
        return number of particles in the current table
        :return: int size
        """
        return self.table_size

    @property  # DEPRECATED, legacy
    def built_types(self):
        return self.built_centers

    @property  # overloading
    def positions(self):
        """
        get positions
        :return: table of arrays of positions
        """
        return self.table

    def contract_table(self):
        """
        contracts the whole table into a single list
        :return:
        """
        while self.table.__len__() > 1:
            self.table[0] = np.append(self.table[0], self.table.pop(), axis=0)

    def expend_table(self):
        counter = 0
        while counter < self.table.__len__():
            for i in range(self.table[counter].__len__()-1):
                self.table.append(np.resize(self.table[counter][-1], new_shape=(1, 3)))
                self.table[counter] = np.delete(self.table[counter], -1, 0)
                self.built_centers.append(self.built_centers[counter])
            counter += 1

    def add_one_particle(self, position, list_number=0, ctype='W'):
        try:
            self.table[list_number] = np.append(self.table[0], np.array([[position[0], position[1], position[2]]]),
                                                axis=0)
        except IndexError:
            if self.table.__len__() == list_number:
                self.table.append(np.array([[position[0], position[1], position[2]]]))
            if ctype not in self.built_centers:
                self.built_centers.append(ctype)
        self.table_size += 1

    def _fix_built_list(self):
        if self.built_centers.__len__() == 1:
            _ct = self.built_centers[0]
            self.built_centers = []
            for i in range(self.positions[0].__len__()):
                self.built_centers.append(_ct)

    @staticmethod
    def __get_rot_mat(cubic_plane):
        """
        rotate self.__table from [0 0 1] face to cubic_plane
        :param cubic_plane: list of length 3
        :return: none
        """
        #  normalize plane orientation
        _cp = Vector(cubic_plane)
        _cp.array /= _cp.norm()

        #  build rotation matrix
        _v = _cp.cross_product_with_z_axis()  # crossprod by 001

        _sl = _v.norm()
        _cl = _cp.i_prod_by001()

        _mat_vx = np.array([[0.0, -_v.z, _v.y], [_v.z, 0.0, -_v.x], [-_v.y, _v.x, 0.0]])
        _id = np.array([[1.0, 0, 0], [0, 1.0, 0], [0, 0, 1.0]])

        if _v.array[0] == 0.0 and _v.array[1] == 0.0:  # surface is [001]
            return _id

        return np.linalg.inv(_id + _mat_vx + (1.0-_cl) / _sl**2 * np.linalg.matrix_power(_mat_vx, 2))

    def center_to_zero(self):
        _mean = [0, 0, 0]
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                for k in range(_mean.__len__()):
                    _mean[k] += self.table[i][j, k]
        for i in range(_mean.__len__()):
            _mean[i] /= self.table_size
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                for k in range(_mean.__len__()):
                    self.table[i][j, k] -= _mean[k]

    def drop_component(self, name):
        """
        Drops a component from the tables
        :param name: name of the centerbead dropped
        :return: none
        """
        drop_index = -1
        for i in range(self.built_centers.__len__()):
            if self.built_centers[i] == name:
                drop_index = i
        if drop_index == -1:
            print 'CenterFile : drop_component(name) : component not found in current tables'
        else:
            self.table_size -= self.table[drop_index].__len__()
            self.table[drop_index] = np.zeros((0, 3))

    def change_units(self, new_units):
        if not issubclass(new_units.__class__, SimUnits):
            raise SyntaxError('specified units are not in a unit class form')
        _mul = new_units.get_length(1.0, self.units.lunit)
        for el_tab in self.table:
            el_tab *= _mul
        for item in self.vx:
            item *= _mul
        for item in self.vy:
            item *= _mul
        for item in self.vz:
            item *= _mul
        self.BoxSize *= _mul * _mul * _mul
        self.vmat *= _mul
        self.units = new_units


class Lattice(CenterFile):
    """
    provides a class for building lattices of colloids. Class provides methods to add particles inside the unit cell by
    use of an offset, a rotation method and a cut method
    """
    def __init__(self,  lattice, surf_plane=None, int_bounds=None, units=None):

        if units is not None and issubclass(units.__class__, SimUnits):
            _units = units
        else:
            _units = SimUnits()

        CenterFile.__init__(self, units=_units)
        self.flags['vertical_slice'] = False
        self.lattice = np.ndarray((3, 3))
        self.parse_input_lattice(lattice)

        self.unit_volume = np.dot(self.lattice[0], np.cross(self.lattice[1], self.lattice[2]))
        self.reciprocal = 2*pi*np.transpose(np.linalg.inv(self.lattice))

        if not (surf_plane is None) and surf_plane.__len__() == 3:
            surf_plane[:] = (x / reduce(gcd, surf_plane) for x in surf_plane)
            r_vec = surf_plane[0] * self.reciprocal[0, :] + surf_plane[1] * self.reciprocal[1, :] + \
                    surf_plane[2] * self.reciprocal[2, :]

            self.rot_mat = get_inverse_rotation_matrix(r_vec)
            self.rot_flag = True
            self.surf_plane = Vector(surf_plane)
            self.n_plane = Vector(r_vec)
        if int_bounds is None:
            self.int_bounds = [4, 4, 4]
        else:
            self.int_bounds = [int_bounds[0] * 2, int_bounds[1] * 2, int_bounds[2] * 2]

    @property
    def rotation_matrix(self):
        """
        return rotation matrix of axis
        :return:
        """
        return self.rot_mat

    @property
    def surface_plane(self):
        """
        returns surface plane
        :return: list
        """
        return list(self.surf_plane.array)

    @surface_plane.setter
    def surface_plane(self, sp):
        if sp.__len__() != 3:
            raise SyntaxError('surface plane length is not 3')
        sp[:] = (x / reduce(gcd, sp) for x in sp)
        rvec = sp[0] * self.reciprocal[0, :] + sp[1] * self.reciprocal[1, :] + sp[0] * self.reciprocal[1, :]
        self.surf_plane = Vector(sp)
        self.rot_mat = get_inverse_rotation_matrix(rvec)

    def parse_input_lattice(self, lattice):
        """
        try to parse the supplied lattice, which can be a constant for cubic lattices, three numbers for orthorhombic or
        nine for other symmetries
        :param lattice:
        :return:
        """
        _e = IndexError('CenterFile : Lattice : parse_input_lattice : cannot parse the supplied lattice')

        if isinstance(lattice, np.ndarray) and lattice.shape == (3, 3):
            self.lattice = lattice
            return
        if isinstance(lattice, float):
            self.lattice = np.array([[lattice, 0, 0], [0, lattice, 0], [0, 0, lattice]], dtype=np.float32)
        elif hasattr(lattice, '__iter__'):
            if isinstance(lattice[0], float):
                self.lattice = np.array([[lattice[0], 0, 0], [0, lattice[1], 0], [0, 0, lattice[2]]], dtype=np.float32)
            elif hasattr(lattice[0], '__iter__'):
                self.lattice = np.array(
                    [[lattice[0][0], lattice[0][1], lattice[0][2]], [lattice[1][0], lattice[1][1], lattice[1][2]],
                     [lattice[2][0], lattice[2][1], lattice[2][2]]], dtype=np.float32)
            else:
                raise _e
        else:
            raise _e

    def rotate_and_cut(self, int_bounds, *args, **kwargs):
        warnings.warn('rotate_and_cut will be deprecated, switch to cut_to_dimensions or make_Z_slice',
                      DeprecationWarning)
        self.cut_to_dimensions(int_bounds)

    def cut_to_dimensions(self, int_bounds):
        """
        rotates the crystal system so that the surface plane face supplied faces the Z direction. The new crystal axes
        are generally trigonal in nature and the crystallinity in Z is not guaranteed unless the rotated axes point
        along Z.

        :param int_bounds: list of length 3 of integer bounds
        :return: none
        """

        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _dump_vec = Vector(self.table[i][j, :])
                _dump_vec.rotate(mat=self.rot_mat)
                self.table[i][j, :] = _dump_vec.array
                del _dump_vec

        _b_x = Vector(self.lattice[0, :])
        _b_y = Vector(self.lattice[1, :])
        _b_z = Vector(self.lattice[2, :])

        _b_x.rotate(mat=self.rot_mat)
        _b_y.rotate(mat=self.rot_mat)
        _b_z.rotate(mat=self.rot_mat)

        #  decomposition matrix
        _mat = np.array([[_b_x.x, _b_y.x, _b_z.x], [_b_x.y, _b_y.y, _b_z.y], [_b_x.z, _b_y.z, _b_z.z]])
        _index_to_keep = []
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _tmp_dump = Vector(list(self.table[i][j, :]))

                #  decompose into base vectors _b_x, _b_y, _b_z
                _a1, _a2, _a3 = np.linalg.solve(_mat, _tmp_dump.array)

                #  check lattice params, need some tolerance check in here
                _tol = 1e-3
                if (-int_bounds[0] + _tol < _a1 <= int_bounds[0] + _tol) \
                        and (-int_bounds[1] + _tol < _a2 <= int_bounds[1] + _tol) \
                        and (-int_bounds[2] + _tol < _a3 <= int_bounds[2] + _tol):
                    _index_to_keep.append([i, j])
                del _tmp_dump

        for i in range(self.table.__len__()):
            _loc_list = []
            for j in range(_index_to_keep.__len__()):
                if _index_to_keep[j][0] == i:
                    _loc_list.append(_index_to_keep[j][1])
            self.table[i] = self.table[i][_loc_list, :]
        # this discards sanity checks, should be careful.
        self.table_size = 0
        for i in range(self.table.__len__()):
            self.table_size += self.table[i].__len__()

        self.vx = list(_b_x.array)
        self.vy = list(_b_y.array)
        self.vz = list(_b_z.array)

        self.vmat = _mat
        self.int_bounds = int_bounds

    def make_Z_slice(self, int_bounds, plane_vectors=None):
        """
        rotates the crystal system so that the surface plane face supplied faces the Z direction. The new crystal axes
        are generally trigonal in nature and the crystallinity in Z is not guaranteed unless the rotated axes point
        along Z. Decomposition in XY plane is made along plane_vectors which are defined as integer sums of b1, b2, b3.

        :param int_bounds: list of length 3 of integer bounds
        :param plane_vectors: list of list of integers
        :return: none
        """

        self.flags['vertical_slice'] = True
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _dump_vec = Vector(self.table[i][j, :])
                _dump_vec.rotate(mat=self.rot_mat)
                self.table[i][j, :] = _dump_vec.array
                del _dump_vec

        _b_x = Vector(self.lattice[0, :])
        _b_y = Vector(self.lattice[1, :])
        _b_z = Vector(self.lattice[2, :])

        _b_x.rotate(mat=self.rot_mat)
        _b_y.rotate(mat=self.rot_mat)
        _b_z.rotate(mat=self.rot_mat)

        if plane_vectors is not None:
            _b_x.array, _b_y.array = plane_vectors[0][0] * _b_x.array + plane_vectors[0][1] * _b_y.array + \
                                     plane_vectors[0][2] * _b_z.array, \
                                     plane_vectors[1][0] * _b_x.array + plane_vectors[1][1] * _b_y.array + \
                                     plane_vectors[1][2] * _b_z.array
            a = np.linalg.norm(self.lattice[0])
            b = np.linalg.norm(self.lattice[1])
            c = np.linalg.norm(self.lattice[2])
            alpha = acos(np.dot(self.lattice[1], self.lattice[2]) / np.linalg.norm(self.lattice[1]) / np.linalg.norm(
                self.lattice[2]))
            beta = acos(np.dot(self.lattice[0], self.lattice[2]) / np.linalg.norm(self.lattice[0]) / np.linalg.norm(
                self.lattice[2]))
            gamma = acos(np.dot(self.lattice[0], self.lattice[1]) / np.linalg.norm(self.lattice[1]) / np.linalg.norm(
                self.lattice[0]))
            dhkl = (b * b * c * c * sin(alpha) ** 2 * self.surf_plane[0] ** 2 +
                    a * a * c * c * sin(beta) ** 2 * self.surf_plane[1] ** 2 +
                    a * a * b * b * sin(gamma) ** 2 * self.surf_plane[2] ** 2 +
                    2.0 * a * b * c * c * (cos(alpha) * cos(beta) - cos(gamma)) * self.surf_plane[0] * self.surf_plane[
                        1] +
                    2.0 * a * a * b * c * (cos(beta) * cos(gamma) - cos(alpha)) * self.surf_plane[1] * self.surf_plane[
                        2] +
                    2.0 * a * b * b * c * (cos(gamma) * cos(alpha) - cos(beta)) * self.surf_plane[0] * self.surf_plane[
                        2]
                    ) ** -0.5 * self.unit_volume
            _b_z.array = np.array(
                [0.0, 0.0, dhkl])
        # decomposition matrix
        _mat = np.array([[_b_x.x, _b_y.x, 0.0], [_b_x.y, _b_y.y, 0.0], [0.0, 0.0, _b_z.z]])

        _index_to_keep = []
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _tmp_dump = Vector(list(self.table[i][j, :]))

                # decompose into base vectors _b_x, _b_y, _b_z
                _a1, _a2, _a3 = np.linalg.solve(_mat, _tmp_dump.array)

                # check lattice params, need some tolerance check in here
                _tol = 1e-3
                if (-int_bounds[0] + _tol < _a1 <= int_bounds[0] + _tol) \
                        and (-int_bounds[1] + _tol < _a2 <= int_bounds[1] + _tol) \
                        and (-int_bounds[2] + _tol < _a3 <= int_bounds[2] + _tol):
                    _index_to_keep.append([i, j])
                del _tmp_dump

        for i in range(self.table.__len__()):
            _loc_list = []
            for j in range(_index_to_keep.__len__()):
                if _index_to_keep[j][0] == i:
                    _loc_list.append(_index_to_keep[j][1])
            self.table[i] = self.table[i][_loc_list, :]
        # this discards sanity checks, should be careful.
        self.table_size = 0
        for i in range(self.table.__len__()):
            self.table_size += self.table[i].__len__()

        self.vx = list(_b_x.array)
        self.vy = list(_b_y.array)
        self.vz = list(_b_z.array)

        self.vmat = _mat
        self.int_bounds = int_bounds

    def make_two_Z_slice(self, int_bounds_top, int_bounds_bottom, plane_vectors=None):
        """
        rotates the crystal system so that the surface plane face supplied faces the Z direction. The new crystal axes
        are generally trigonal in nature and the crystallinity in Z is not guaranteed unless the rotated axes point
        along Z. Decomposition in XY plane is made along plane_vectors which are defined as integer sums of b1, b2, b3.

        :param int_bounds_top: list of length 3 of bounds at the top of the crystal
        :param int_bounds_bottom list of length 3 of bounds at the bottom of the crystal
        :param plane_vectors: list of list of integers
        :return: none
        """

        self.flags['vertical_slice'] = True
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _dump_vec = Vector(self.table[i][j, :])
                _dump_vec.rotate(mat=self.rot_mat)
                self.table[i][j, :] = _dump_vec.array
                del _dump_vec

        _b_x = Vector(self.lattice[0, :])
        _b_y = Vector(self.lattice[1, :])
        _b_z = Vector(self.lattice[2, :])

        _b_x.rotate(mat=self.rot_mat)
        _b_y.rotate(mat=self.rot_mat)
        _b_z.rotate(mat=self.rot_mat)

        if plane_vectors is not None:
            _b_x.array, _b_y.array = plane_vectors[0][0] * _b_x.array + plane_vectors[0][1] * _b_y.array + \
                                     plane_vectors[0][2] * _b_z.array, \
                                     plane_vectors[1][0] * _b_x.array + plane_vectors[1][1] * _b_y.array + \
                                     plane_vectors[1][2] * _b_z.array
            a = np.linalg.norm(self.lattice[0])
            b = np.linalg.norm(self.lattice[1])
            c = np.linalg.norm(self.lattice[2])
            alpha = acos(np.dot(self.lattice[1], self.lattice[2]) / np.linalg.norm(self.lattice[1]) / np.linalg.norm(
                self.lattice[2]))
            beta = acos(np.dot(self.lattice[0], self.lattice[2]) / np.linalg.norm(self.lattice[0]) / np.linalg.norm(
                self.lattice[2]))
            gamma = acos(np.dot(self.lattice[0], self.lattice[1]) / np.linalg.norm(self.lattice[1]) / np.linalg.norm(
                self.lattice[0]))
            dhkl = (b * b * c * c * sin(alpha) ** 2 * self.surf_plane[0] ** 2 +
                    a * a * c * c * sin(beta) ** 2 * self.surf_plane[1] ** 2 +
                    a * a * b * b * sin(gamma) ** 2 * self.surf_plane[2] ** 2 +
                    2.0 * a * b * c * c * (cos(alpha) * cos(beta) - cos(gamma)) * self.surf_plane[0] * self.surf_plane[1] +
                    2.0 * a * a * b * c * (cos(beta) * cos(gamma) - cos(alpha)) * self.surf_plane[1] * self.surf_plane[2] +
                    2.0 * a * b * b * c * (cos(gamma) * cos(alpha) - cos(beta)) * self.surf_plane[0] * self.surf_plane[2]
                    ) ** -0.5 * self.unit_volume
            _b_z.array = np.array(
                [0.0, 0.0, dhkl])
        # decomposition matrix
        _mat = np.array([[_b_x.x, _b_y.x, 0.0], [_b_x.y, _b_y.y, 0.0], [0.0, 0.0, _b_z.z]])

        _index_to_keep = []
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _tmp_dump = Vector(list(self.table[i][j, :]))

                # decompose into base vectors _b_x, _b_y, _b_z
                _a1, _a2, _a3 = np.linalg.solve(_mat, _tmp_dump.array)

                # check lattice params, need some tolerance check in here
                _tol = 1e-3
                if (-int_bounds_bottom[0] + _tol < _a1 <= int_bounds_top[0] + _tol) \
                        and (-int_bounds_bottom[1] + _tol < _a2 <= int_bounds_top[1] + _tol) \
                        and (-int_bounds_bottom[2] + _tol < _a3 <= int_bounds_top[2] + _tol):
                    _index_to_keep.append([i, j])

                del _tmp_dump

        for i in range(self.table.__len__()):
            _loc_list = []
            for j in range(_index_to_keep.__len__()):
                if _index_to_keep[j][0] == i:
                    _loc_list.append(_index_to_keep[j][1])
            self.table[i] = self.table[i][_loc_list, :]
        # this discards sanity checks, should be careful.
        self.table_size = 0
        for i in range(self.table.__len__()):
            self.table_size += self.table[i].__len__()

        self.vx = list(_b_x.array)
        self.vy = list(_b_y.array)
        self.vz = list(_b_z.array)

        self.vmat = _mat
        self.int_bounds = [int_bounds_top[0], int_bounds_top[1], (int_bounds_bottom[2] + int_bounds_top[2])/2.0]

    def make_all_slices(self, max_inner_product, plane_vectors):

        _b_x = Vector(self.lattice[0, :])
        _b_y = Vector(self.lattice[1, :])
        _b_z = Vector(self.lattice[2, :])

        # find which values to keep
        _index_to_keep = []
        _mat = np.array([[_b_x.x, _b_y.x, _b_z.x], [_b_x.y, _b_y.y, _b_z.y], [_b_x.z, _b_y.z, _b_z.z]])
        for i in range(self.table.__len__()):
            for j in range(self.table[i].__len__()):
                _tmp_dump = Vector(list(self.table[i][j, :]))
                # decompose into base vectors _b_x, _b_y, _b_z
                _a1, _a2, _a3 = np.linalg.solve(_mat, _tmp_dump.array)
                tol = 1e-3
                to_remove = False
                for plane in plane_vectors:
                    iprod = _a1 * plane[0] + _a2 * plane[1] + _a3 * plane[2]
                    if iprod > max_inner_product:
                        to_remove = True
                        break
                if not to_remove:
                    _index_to_keep.append([i, j])

        # clean the table
        for i in range(self.table.__len__()):
            _loc_list = []
            for j in range(_index_to_keep.__len__()):
                if _index_to_keep[j][0] == i:
                    _loc_list.append(_index_to_keep[j][1])
            self.table[i] = self.table[i][_loc_list, :]
        # this discards sanity checks, should be careful.
        self.table_size = 0
        for i in range(self.table.__len__()):
            self.table_size += self.table[i].__len__()

    def add_particles_on_lattice(self, center_type, offset):
        """
        Adds particle on a orthorhombic lattice, with offset as a way to decorate (i.e. make FCC crystals), each offset
        adds one particle per unit cell;

        Useful decorators (cubic lattice)

        BCC : [0.5, 0.5, 0.5]
        FCC : [0.5, 0.5, 0], plus permutations, 3 offsets
        Diamond : FCC + [1/4, 1/4, 1/4]


        :param center_type: type of center built, will try to match previously built ones
        :param offset: list of length 3 in units of [vx, vy, vz] which are the base lattice vectors
        :return: None
        """

        new_table = np.zeros(((2*self.int_bounds[0]+1) * (2*self.int_bounds[1]+1) * (2*self.int_bounds[2]+1), 3),
                             dtype=np.float32)

        for _ in range(new_table.shape[0]):
            new_table[_, :] += offset[0] * self.lattice[0, :] + offset[1] * self.lattice[1, :] + \
                               offset[2] * self.lattice[2, :]

        _ = 0
        for _a1 in range(-self.int_bounds[0], self.int_bounds[0] + 1):
            for _a2 in range(-self.int_bounds[1], self.int_bounds[1] + 1):
                for _a3 in range(-self.int_bounds[2], self.int_bounds[2] + 1):
                    new_table[_, :] += _a1 * self.lattice[0, :] + _a2 * self.lattice[1, :] + _a3 * self.lattice[2, :]
                    _ += 1

        is_built = False
        for i in range(self.built_centers.__len__()):
            if self.built_centers[i] == center_type:
                self.table[i] = np.append(self.table[i], new_table, axis=0)
                self.table_size += new_table.__len__()
                is_built = True

        if not is_built:
            self.table.append(new_table)
            self.table_size += new_table.__len__()
            self.built_centers.append(center_type)

    def add_random_positions(self, center_type=None, N=0, size=None):
        """
        Adds N positions with center_type defined as the center. If this is already defined somewhere else (incl on lattice
        sites), this assumes objects built are identical
        :param center_type: hoomd type name, defaults to 'W'
        :param N: number of bodies, defaults to 0
        :param size: excluded volume size of the object. If an iterable is passed in, this assumes that it describes the
        excluded volumes of all building blocks
        :return: None
        """
        if size is None:
            size = [0 for i in range(self.built_centers.__len__() + 1)]
        elif isinstance(size, float):
            size = [0 for i in range(self.built_centers.__len__())] + [size]
        elif hasattr(size, '__iter__') and size.__len__() >= self.built_centers.__len__():
            pass
        else:
            raise SyntaxError('CenterFile : add_random_positions : Unable to deal with the size input, expects None, '
                              'float or list of length greater or equal to number of build objects, got ' + str(type(size).__name__))

        is_built = False
        size_index = self.built_centers.__len__()
        for i in range(self.built_centers.__len__()):
            if self.built_centers[i] == center_type:
                is_built = True
                size_index = i

        if not is_built and size.__len__() == self.built_centers.__len__():
            raise SyntaxError('CenterFile : add_random_positions : length of size is equal to length of built_centers, '
                              'but the added particle is not built. Unable to determine it''s size !')

        added_table = np.zeros((0, 3), dtype=np.float32)

        # counting variables
        Padd = 0 # added particles
        current_trial = 0 # number of trials on the current particle
        Nfailed = 0 # number of trials of the whole method

        while Padd < N:
            # generate a random position
            gpos = np.zeros((1, 3), dtype=np.float32)
            for dim in range(3):
                gpos += np.reshape(np.random.uniform(-self.int_bounds[dim], self.int_bounds[dim]) * self.lattice[dim, :], (1,3))

            # check whether that position is actually valid
            reject = False
            for table_idx in range(self.table.__len__()):
                for position_idx in range(self.table[table_idx].__len__()):
                    dist = np.linalg.norm(np.array(self.table[table_idx][position_idx]) - gpos)
                    if(dist < (size[size_index] + size[table_idx]) / 2.0):
                        reject = True
                        break
                if reject:
                    break

            if not reject:
                Padd += 1
                current_trial = 0
                added_table = np.append(added_table, gpos, axis=0)
            else:
                current_trial += 1

            if current_trial > 1000:
                if(Nfailed > 1000):
                    raise RuntimeError('CenterFile : Lattice : Unable to insert all requested particles ')
                Nfailed += 1
                Padd = 0
                current_trial = 0
                added_table = np.zeros((0, 3), dtype=np.float32)

        if is_built:
            self.table[size_index] = np.append(self.table[size_index], added_table, axis=0)
            self.table_size += added_table.__len__()
        else:
            self.table.append(added_table)
            self.table_size += added_table.__len__()
            self.built_centers.append(center_type)

    def gather_slices(self):
        local_table = copy.deepcopy(self.table)
        for cidx in range(local_table.__len__()):
            for j in range(self.table[cidx].__len__()):
                _dump_vec = Vector(self.table[cidx][j, :])
                _dump_vec.rotate(mat=self.rot_mat)
                local_table[cidx][j, :] = _dump_vec.array
                del _dump_vec

        a = np.linalg.norm(self.lattice[0])
        b = np.linalg.norm(self.lattice[1])
        c = np.linalg.norm(self.lattice[2])
        alpha = acos(np.dot(self.lattice[1], self.lattice[2]) / np.linalg.norm(self.lattice[1]) / np.linalg.norm(self.lattice[2]))
        beta = acos(np.dot(self.lattice[0], self.lattice[2])/ np.linalg.norm(self.lattice[0]) / np.linalg.norm(self.lattice[2]))
        gamma = acos(np.dot(self.lattice[0], self.lattice[1])/ np.linalg.norm(self.lattice[1]) / np.linalg.norm(self.lattice[0]))

        dhkl = (b * b * c * c * sin(alpha) ** 2 * self.surf_plane[0] ** 2 +
                a * a * c * c * sin(beta) ** 2 * self.surf_plane[1] ** 2 +
                a * a * b * b * sin(gamma) ** 2 * self.surf_plane[2] ** 2 +
                2.0 * a * b * c * c * (cos(alpha) * cos(beta) - cos(gamma)) * self.surf_plane[0] * self.surf_plane[1] +
                2.0 * a * a * b * c * (cos(beta) * cos(gamma) - cos(alpha)) * self.surf_plane[1] * self.surf_plane[2] +
                2.0 * a * b * b * c * (cos(gamma) * cos(alpha) - cos(beta)) * self.surf_plane[0] * self.surf_plane[2]
                ) ** -0.5 * self.unit_volume
        # decomposition matrix
        _mat = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, dhkl]])
        a3list = []
        for cidx in range(local_table.__len__()):
            la3 = []
            for j in range(self.table[cidx].__len__()):
                _tmp_dump = Vector(list(local_table[cidx][j, :]))
                _a1, _a2, _a3 = np.linalg.solve(_mat, _tmp_dump.array)
                la3.append(_a3)
            a3list.append(uniquetol(np.array(la3)))
        return a3list


class RandomPositions(CenterFile):
    """
    Sets up randomly positioned centers in a given box of system size x system size x system size. Assumes that list
    of properties are passed in
    """
    def __init__(self, system_size, particle_numbers, shape_objects=None, sizes=None, centertags=None,
                 units=None, constraint=None):
        """
        Creates a random configuration.
        :param system_size: Box edge length, float
        :param particle_numbers: list of integers of types
        :param shape_objects: for checking whether safe distances are defined, list of shapes
        :param sizes: list of sizes to multiply the safe distance with
        :param centertags: tags of the centers
        """
        if units is not None and issubclass(units.__class__, SimUnits):
            _units = units
        else:
            _units = SimUnits()

        CenterFile.__init__(self, units=_units)
        if hasattr(system_size, '__iter__'):
            self.BoxSize = system_size[0] * system_size[1] * system_size[2]
        else:
            self.BoxSize = system_size**3
        self.int_bounds = [0.5, 0.5, 0.5]
        Table = []  # kth table contains (num_part(k),3) array of the kth particle type
        self.rand_flag = True
        self.table = []
        self.table_size = 0
        if not hasattr(system_size, '__iter__'):
            self.lattice = np.array([[system_size, 0, 0], [0, system_size, 0], [0, 0, system_size]], dtype=np.float32)
            self.vx = [system_size, 0.0, 0.0]
            self.vy = [0.0, system_size, 0.0]
            self.vz = [0.0, 0.0, system_size]
            self.vmat = np.array([self.vx, self.vy, self.vz])
        else:
            self.lattice = np.array([[system_size[0], 0, 0], [0, system_size[1], 0], [0, 0, system_size[2]]], dtype=np.float32)
            self.vx = [system_size[0], 0.0, 0.0]
            self.vy = [0.0, system_size[1], 0.0]
            self.vz = [0.0, 0.0, system_size[2]]
            self.vmat = np.array([self.vx, self.vy, self.vz])

        local_min_dist = np.zeros(particle_numbers.__len__(), dtype=np.float32)
        if shape_objects is not None:
            for _ in range(particle_numbers.__len__()):
                try:
                    local_min_dist[_] = shape_objects[_].flags['hard_core_safe_dist']
                except KeyError:
                    pass
                except IndexError:
                    print 'CenterFile : RandomPositions : Size of shape array smaller than size of particle types'

        if sizes is None:
            sizes = np.ones(particle_numbers.__len__(), dtype=np.float32) # TODO : change this to be indep of unit system
            if shape_objects is not None:
                for p in range(shape_objects.__len__()):
                    # TODO : make sure this object unit and shape object units match
                    if 'size' in shape_objects[p].flags:
                        sizes[p] = shape_objects[p].flags['size']

        if centertags is None:
            centertags = ['W']*particle_numbers.__len__()

        j = 0
        while j < particle_numbers.__len__():
            Table.append(np.zeros((particle_numbers[j], 3)))
            toplist_current_try = 0
            PNum = 0

            while PNum < particle_numbers[j]:

                current_try = 0
                curr_list = True

                while curr_list:
                    Table[j][PNum, 0] = self.vx[0] * random.uniform(-0.5, 0.5)
                    Table[j][PNum, 1] = self.vy[1] * random.uniform(-0.5, 0.5)
                    Table[j][PNum, 2] = self.vz[2] * random.uniform(-0.5, 0.5)

                    curr_list = False
                    for i in range(Table.__len__()-1):
                        for k in range(Table[i].__len__()):
                            value = (((Table[j][PNum, 0] - Table[i][k, 0])**2 + (Table[j][PNum, 1] - Table[i][k, 1])**2
                                      + (Table[j][PNum, 2] - Table[i][k, 2])**2)**0.5)
                            curr_list = curr_list or value < (sizes[i] * local_min_dist[i] + sizes[j] * local_min_dist[j]) / 2.0

                    for i in range(0, PNum):
                        value = (((Table[j][PNum, 0] - Table[j][i, 0])**2 + (Table[j][PNum, 1] - Table[j][i, 1])**2 +
                                  (Table[j][PNum, 2] - Table[j][i, 2])**2)**0.5)
                        curr_list = curr_list or value < sizes[j] * local_min_dist[j]

                    if constraint is not None:
                        curr_list = curr_list or not constraint([Table[j][Pnum, 0], Table[j][Pnum, 1], Table[j][Pnum, 2]])

                    current_try += 1
                    if current_try > 1000:
                        PNum = 0
                        Table[j] = np.zeros((particle_numbers[j], 3), dtype=np.float32)
                        current_try = 0
                        toplist_current_try += 1
                        if toplist_current_try > 1000:
                            j = 0
                            Table = [np.zeros((particle_numbers[j], 3), dtype=np.float32)]
                            toplist_current_try = 0
                PNum += 1
            j += 1

        table_size = 0
        for i in range(Table.__len__()):
            table_size = table_size + Table[i].__len__()
            self.table.append(Table[i])

        self.table_size += table_size
        for i in range(particle_numbers.__len__()):
            self.built_centers.append(centertags[i])

