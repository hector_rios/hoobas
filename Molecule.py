"""
File provides implementation of atomic force-fields. Wrapper to Build is provided by Colloid which imports the molecules
as colloids for the builder
"""

import numpy as np
from Units import SimulationUnits as SimUnits
from math import *
from CoarsegrainedBead import bead as cg_bead
from Quaternion import Quat
from Util import Vector


class Molecule(object):
    def __init__(self, units=None):
        self.positions = np.zeros((0, 3))
        if units is None:
            self.units = SimUnits()
            self.units.set_length('nm')
            self.units.set_energy('kJ/mol')
            self.units.set_mass('amu')
        else:
            self.units = units

        self.mass = 0.0

        self.beads = []
        self.bonds = []
        self.angles = []
        self.dihedrals = []
        self.impropers = []
        self.wdv = []
        self.flags = {}

        self.quaternion = Quat(np.eye(3))
        self.itensor = np.zeros((1, 3), dtype=np.float32)

    def set_rigid(self):
        self.move_center_mass()
        self.set_geometric_quaternion()
        self.flags['simple_I_tensor'] = False
        self.flags['I_tensor'] = self.itensor

        md = 0.0
        for pos in self.positions:
            md = max(md, np.linalg.norm(pos))
        self.flags['hard_core_safe_dist'] = md

    def get_itensor(self):
        return self.itensor

    def get_quaternion(self):
        return self.quaternion

    def set_ua(self):
        pass

    def set_lincs_bonds(self):
        raise NotImplementedError('Rigid bond model is not implemented yet')

    def get_mass(self):
        _ = 0.0
        for p in self.beads:
            _ += p.mass
        return _

    def move_center_mass(self):
        cmass = np.zeros((1, 3), dtype=np.float32)
        for p in self.beads:
            cmass += p.position * p.mass
        cmass /= self.mass
        self.positions -= cmass
        for p in self.beads:
            p.position -= cmass[0, :]

    def rotate_object(self, operator):
        if operator is None:
            operator = np.eye(3)
        _qop = Quat(operator)
        self.quaternion = _qop * self.quaternion

    def set_geometric_quaternion(self, tol_eps_rel=2.0):
        """
        calculates the tensor of the structure in self.table; uses diagonalization methods and has numerical precision
        issues
        :return:
        """
        inertia_tensor = np.zeros((3, 3), dtype=np.longfloat)
        for bead in self.beads:
            inertia_tensor[0, 0] += bead.mass * (bead.position[1] * bead.position[1] +
                                                 bead.position[2] * bead.position[2])
            inertia_tensor[1, 1] += bead.mass * (bead.position[0] * bead.position[0] +
                                                 bead.position[2] * bead.position[2])
            inertia_tensor[2, 2] += bead.mass * (bead.position[0] * bead.position[0] +
                                                 bead.position[1] * bead.position[1])
            inertia_tensor[0, 1] -= bead.position[0] * bead.position[1]
            inertia_tensor[0, 2] -= bead.position[0] * bead.position[2]
            inertia_tensor[1, 2] -= bead.position[1] * bead.position[2]
        inertia_tensor[1, 0] = inertia_tensor[0, 1]
        inertia_tensor[2, 0] = inertia_tensor[0, 2]
        inertia_tensor[2, 1] = inertia_tensor[1, 2]

        #####################################
        # numerical fixing of some low values
        #####################################
        inertia_tensor = inertia_tensor.astype(dtype=np.float32)
        _maxel = np.max(inertia_tensor)
        for dim1 in range(3):
            for dim2 in range(3):
                if abs(inertia_tensor[dim1, dim2]) < tol_eps_rel * np.finfo(np.float32).eps * _maxel:
                    inertia_tensor[dim1, dim2] = 0.0
        w, v = np.linalg.eigh(inertia_tensor / self.positions.__len__())

        # sort the eigenvalues
        idx = w.argsort(kind='mergesort')[::-1]
        w = w[idx]
        v = v[:, idx]
        # invert one axis if the coordinate system has become left-handed
        if np.linalg.det(v) < 0:
            v[:, 0] = -v[:, 0]
        self.quaternion = Quat(v)

        for idx in range(0, self.beads.__len__()):
            _vr = Vector(self.beads[idx].position)
            _mat = self.quaternion.transform
            _vr.rotate(_mat)
            self.beads[idx].position = _vr.array
            self.positions[idx] = _vr.array

        self.itensor = np.array([w[0], w[1], w[2]], dtype=np.float32)

    def set_properties(self, properties=None):
        if not (properties is None):
            self.flags.update(properties)
        self.flags['pdb_object'] = True

    def change_units(self, new_units):
        pass


class UAParams(object):
    _atoms = {}
    _bonds = {}
    _angles = {}
    _dihedrals = {}
    _impropers = {}
    _max_ring = 0

    def __init__(self):
        pass

    def get_neighbours(self, idx):
        pass

    def get_nth_neighbours(self, idx, n):
        pass

    def is_ring(self, idx):
        return False


class TraPPEUA(UAParams):
    _atoms = {}
    _bonds = {}
    _angles = {}
    _dihedrals = {}
    _impropers = {}
    _max_ring = 6

    def __init__(self):
        super(TraPPEUA, self).__init__()

    def parse_structure_interactions(self):
        pass


class WaterSPCE(Molecule):
    def __init__(self):
        _units = SimUnits()
        _units.set_length('Ang')
        _units.set_energy('kJ/mol')
        _units.set_mass('amu')

        super(WaterSPCE, self).__init__(units=_units)

        self.positions = np.array([[0.0, 0.0, 0.0],
                                   [cos(radians(109.47/2.0)), -sin(radians(109.47/2.0)), 0.0],
                                   [cos(radians(109.47/2.0)), sin(radians(109.47/2.0)), 0.0]], dtype=np.float32)
        self.beads.append(cg_bead(position=self.positions[0, :], beadtype='SPCEO', charge=-0.8476, mass=16.0,
                                  diameter=3.16555529527509, atomic_eps=0.649597253336511, body=0))
        self.beads.append(cg_bead(position=self.positions[1, :], beadtype='SPCEH', charge=0.8476 / 2.0, mass=1.0,
                                  diameter=0.0, atomic_eps=0.0, body=0))
        self.beads.append(cg_bead(position=self.positions[2, :], beadtype='SPCEH', charge=0.8476 / 2.0, mass=1.0,
                                  diameter=0.0, atomic_eps=0.0, body=0))
        self.mass = 18.0
        self.wdv = [{'type': 'SPCEO', 'eps': 0.649597253336511, 'sigma': 3.16555529527509},
                    {'type': 'SPCEH', 'eps': 0.0, 'sigma': 0.0001}]

    def set_ua(self):
        raise SyntaxError('Trying to set SPC/E water to non-rigid, wrong atomic constructor')

    def set_lincs_bonds(self):
        raise SyntaxError('Trying to set SPC/E water to non-rigid, wrong atomic constructor')


class UA_TraPPE_AtomicPSS(Molecule, TraPPEUA):
    def __init__(self, nmonomer):
        _units = SimUnits()
        _units.set_length('Ang')
        _units.set_energy('kJ/mol')
        _units.set_mass('amu')
        super(UA_TraPPE_AtomicPSS, self).__init__(_units)

    def set_rigid(self):
        raise SyntaxError('Setting a polymer to be fully rigid, aborting')

    def set_ua(self):
        pass


class UA_TraPPE_AtomicPDADMA(Molecule, TraPPEUA):
    def __init__(self, nmonomer):
        _units = SimUnits()
        _units.set_length('Ang')
        _units.set_energy('kJ/mol')
        _units.set_mass('amu')
        super(UA_TraPPE_AtomicPDADMA, self).__init__(_units)

    def set_rigid(self):
        raise SyntaxError('Setting a polymer to be fully rigid, aborting')
