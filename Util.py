"""
    collection of methods / classes used by many other objects
"""

import numpy as np
import random


class Vector(object):
    """
    simple Vector class with useful tools; should be reusable
    """

    def __init__(self, array, ntol=1e-5):
        self.array = np.array(array, dtype=np.float32)
        self.ntol = ntol
        if array.__len__() < 1:
            del self

    def __getitem__(self, item):
        return self.array[item]

    def __add__(self, other):
        return Vector(self.array + other.array)

    def __mul__(self, other):
        return Vector(self.array * other)

    def __neg__(self):
        return Vector(-self.array)

    def __sub__(self, other):
        return Vector(self.array - other.array)

    @property
    def x(self):
        return self.array[0]

    @property
    def y(self):
        return self.array[1]

    @property
    def z(self):
        return self.array[2]

    def norm(self):
        """Return the Euclidean norm of the array"""
        return np.sqrt(sum(self.array*self.array))

    def normalize(self):
        """Normalize the Vector so that the magnitude is 1"""
        self.array /= self.norm()

    def cross_product_with_z_axis(self):
        return Vector([-self.array[1], self.array[0], 0.0])

    def i_prod_by001(self):
        """Dot product with z-axis?"""
        return self.array[2]

    def rotate(self, mat):
        self.array = mat.dot(self.array)

    def inverse_rotation(self, mat):
        self.array = np.linalg.inv(mat).dot(self.array)

    def s_proj_to_xy(self, vz):
        """
        Projects the Vector onto the (x,y) plane and then rescales the Vector
        by |vz| / Vector.z if Vector.z > 0.

        What is vz?
        Why would this operation be used?
        """
        if not(abs(self.array[2]) < self.ntol):
            self.array *= vz.norm() / (self.array[2])
            self.array[2] = 0.0


def read_centers(centers_file_path):
    """
    legacy, remove this soon
    :param centers_file_path:
    :return:
    """
    positions = []
    types = []
    with open(centers_file_path, "r") as input_file:
        for line_number, line in input_file:
            if line_number < 2:
                continue
            type_, x, y, z = line.strip().split()
            position = [float(x), float(y), float(z)]
            positions.append(position)
            types.append(type_)
    return types, positions


def get_rotation_matrix(cubic_plane):
    """Needs a description of what rotation matrix it is calculating and what cubic_plane is"""
    plane_norm = Vector(cubic_plane)
    plane_norm.normalize()

    v = plane_norm.cross_product_with_z_axis()

    if v.array[0] == 0.0 and v.array[1] == 0.0:  # surface is [001]
        return np.eye(3)
    else:
        cl = plane_norm.i_prod_by001()
        mat_vx = np.array([[0.0, -v.z, v.y],
                           [v.z, 0.0, -v.x],
                           [-v.y, v.x, 0.0]])
        # Where does this equation come from?
        return np.eye(3) + mat_vx + (1.0 - cl) / v.norm() ** 2 * np.linalg.matrix_power(mat_vx, 2)


def get_inverse_rotation_matrix(cubic_plane):
    return np.linalg.inv(get_rotation_matrix(cubic_plane))


def gen_random_mat():
    theta = random.uniform(0, 2*np.pi)
    z = random.uniform(0, 1)
    surface_norm = [(1-z**2)**0.5*np.cos(theta),
                    (1-z**2)**0.5*np.sin(theta),
                    z]
    return get_rotation_matrix(surface_norm)


def is_diagonal(lattice):
    """I'm really not sure what this is supposed to do.
    For example x = [[1, 0, 0],
                     [0, 1, 0],
                     [0, 0, 1]]
    np.diag(x) = [1, 1, 1]
    np.diag(x) == x = [[True, False, False],
                       [False, True, False],
                       [False, False, True]]
    (np.diag(x) == x).all() = False
    Therefore the identity matrix is not a diagonal matrix"""
    return (np.diag(lattice) == lattice).all()


def is_cubic(lattice):
    diag = np.diag(lattice)
    return is_diagonal(lattice) and (diag[0] == diag[1] == diag[2])


def c_hoomd_box(v, int_bounds, z_multi=1.00):
    """What does this function do?
    What is v?
    What is int_bounds?
    What is z_multi?"""
    vx = v[0][:]
    vy = v[1][:]
    vz = v[2][:]

    for i in range(len(vx)):
        vx[i] *= 2 * int_bounds[0]
        vy[i] *= 2 * int_bounds[1]
        vz[i] *= 2 * int_bounds[2] * z_multi

    lx = (np.dot(vx, vx)) ** 0.5
    a2x = np.dot(vx, vy) / lx
    ly = (np.dot(vy, vy) - a2x ** 2.0) ** 0.5
    xy = a2x / ly
    v0xv1 = np.cross(vx, vy)
    v0xv1mag = np.sqrt(np.dot(v0xv1, v0xv1))
    lz = np.dot(vz, v0xv1) / v0xv1mag
    # if lz < 0.0:
    #    vz = -np.array(vz)
    #    lz = np.dot(vz, v0xv1) / v0xv1mag
    a3x = np.dot(vx, vz) / lx
    xz = a3x / lz
    yz = (np.dot(vy, vz) - a2x * a3x) / (ly * lz)

    return [lx, ly, lz, xy, xz, yz]


def get_v1_v2_rotmat(initial, final):
    """Doesn't appear to be used.
    What does it do?
    What is initial?
    What is final?"""
    vec_f = np.array(final, dtype=np.float32)
    vec_i = np.array(initial, dtype=np.float32)

    vec_f /= np.linalg.norm(vec_f)
    vec_i /= np.linalg.norm(vec_i)

    v = np.cross(vec_f, vec_i)
    mat_vx = np.array([[0.0, -v[2], v[1]],
                       [v[2], 0.0, v[0]],
                       [-v[1], v[0], 0.0]], dtype=np.float32)
    id3 = np.eye(3, 3, dtype=np.float32)
    if abs(np.linalg.norm(v)) < np.finfo(np.float32).eps * 10.0:
        fmax = vec_f.flat[abs(vec_f).argmax()]
        imax = vec_i.flat[abs(vec_i).argmax()]
        return id3 * np.sign(fmax / imax)
    if v[0] == 0.0 and v[1] == 0.0:
        return id3
    else:
        return id3 + mat_vx + (1.0 - np.dot(vec_f, vec_i)) / np.linalg.norm(v)**2 * np.linalg.matrix_power(mat_vx, 2)


def solve_diophantine(a, b, c):
    """
    diophantine equation solver, works, but doesn't guarantee nicest result to the equations
    Finds integer solutions to ax + by = c?
    Is there a reference for this algorithm? (e.g. How it works?)

    :param a:
    :param b:
    :param c:
    :return:
    """
    m1 = 1
    m2 = 0
    n1 = 0
    n2 = 1
    r1 = a
    r2 = b
    while r1 % r2 != 0:
        q = r1 / r2
        aux = r1 % r2
        r1 = r2
        r2 = aux
        aux3 = n1 - n2 * q
        aux2 = m1 - m2 * q
        m1 = m2
        n1 = n2
        m2 = aux2
        n2 = aux3
    return m2 * c, n2 * c


def find_next(input_list, current, value, prop=None):
    """Doesn't appear to be used...
    Appears to go through a sequence, and return the first index of value > current
    Specifying prop causes it look for the first element x of the list such that
    x[prop] == value

    :param input_list:
    :param current:
    :param value:
    :param prop:
    :return:
    """
    if current is None or value is None:
        return None

    if prop is None:
        for idx in range(current+1, len(input_list)):
            if input_list[idx] == value:
                return idx
    else:
        for idx in range(current+1, len(input_list)):
            if getattr(input_list[idx], prop) == value:
                return idx
    return None


def uniquetol(input, relative_tolerance = 1e-5):
    # code lifted from divakar on stackoverflow :
    # https://stackoverflow.com/questions/37847053/uniquify-an-array-list-with-a-tolerance-in-python-uniquetol-equivalent
    # meant to mimic MATLAB uniquetol function

    tol = max(map(lambda x : abs(x), input)) * relative_tolerance
    return input[~(np.triu(np.abs(input[:,None] - input) <= tol,1)).any(0)]


def find_nearest(array,value):
    # code lifted from unutbu on stackoverflow :
    # https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    idx = (np.abs(array-value)).argmin()
    return array[idx]