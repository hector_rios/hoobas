#coding: utf-8
import warnings
import numpy as np


class bead(object):
    """
    This is a coarse grained bead
    """

    def __init__(self, position, beadtype='notype', mass=1.0, body=-1, image=None, charge=0.0,
                 diameter=0.0, quaternion=None, moment_inertia=None, atomic_eps=0.0):

        self.type = beadtype
        self.mass = mass
        self.position = np.array(position, dtype=np.single)
        self.body = body
        self.atomic_eps = 0.0
        if image is None:
            self.image = np.array([0, 0, 0], dtype=np.int32)
        else:
            self.image = np.array(image, dtype=np.int32)
        self.charge = charge
        self.diameter = diameter
        if quaternion is None:
            self.orientation = np.array([1.0, 0.0, 0.0, 0.0], dtype=np.single)
        else:
            try:
                self.orientation = quaternion.q_w_ijk
            except AttributeError:  # In case whatever is passed doesnt support q_w_ijk;
                warnings.warn(
                    'the quaternion passed doesnt support q_w_ijk; errors may be encountered while printing XML',
                    UserWarning)
                self.orientation = quaternion
        if moment_inertia is None:
            self.moment_inertia = np.array([0.0, 0.0, 0.0], dtype=np.single)
        else:
            self.moment_inertia = np.array(moment_inertia, dtype=np.single)
        # things to feed the snapshot data
        self.ret_prop_list = ['mass', 'position', 'body', 'image', 'charge', 'diameter', 'orientation', 'moment_inertia']

    @property
    def beadtype(self):
        return self.type

    # iterable for snapshot feed
    def __iter__(self):
        for prop in self.ret_prop_list:
            yield (prop, getattr(self, prop))

    def change_length_unit(self, multiplier):
        self.position *= multiplier
        self.diameter *= multiplier
        self.charge *= multiplier**0.5
        self.moment_inertia *= multiplier**2.0

    def change_mass_unit(self, multiplier):
        self.mass *= multiplier
        self.moment_inertia *= multiplier

    def change_energy_unit(self, multiplier):
        self.charge *= multiplier**0.5
        self.atomic_eps *= multiplier

    def change_units(self, m_mass, m_length, m_energy):
        self.change_mass_unit(m_mass)
        self.change_energy_unit(m_energy)
        self.change_length_unit(m_length)

    def __str__(self):
        return self.beadtype