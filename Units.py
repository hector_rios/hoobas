# coding: utf-8
import math


class SimulationUnits(object):
    """
    Implement unit context for the simulation so each object can set its units and proper conversion is taken
    care of.

    Three class dictionaries are defined that are shared among all Units instances. They represent the length
    conversions, mass conversions and energy conversions. By using shared dictionaries among all classes
    any addition of units or changes by one instance will be used by all instances

    Units are defined with respect to reduced units R.
    For example, by setting R = 2.0 nm, then 16 nm = 8 R

    The default simulation units are:
     length = 2.0 nm
     mass = 650 Dalton
     energy = 2.5/1.5 kJ/mol (assuming kT = 1.5)
    """

    # Dictionaries to convert between length, mass, and energy
    _length_conversions = {'R': 1.0,
                           'nm': 2.0,
                           'A': 20.0,
                           'Ang': 20.0}
    _mass_conversions = {'R': 1.0,
                         'amu': 650.0}
    _energy_conversions = {'R': 1.0,
                           'kJ/mol': 2.5 / 1.5,
                           'kT': 1.0 / 1.5,
                           'kcal/mol': 2.5 / 1.5 / 4.18}

    # units have to be defined against the reduced unit system; dictionary are static class-wide object, any added unit
    # is synched to all other units

    @property
    def length_conversions(self):
        return self.__class__._length_conversions

    @length_conversions.setter
    def length_conversions(self, val):
        self.__class__._length_conversions = val

    @property
    def mass_conversions(self):
        return self.__class__._mass_conversions

    @mass_conversions.setter
    def mass_conversions(self, val):
        self.__class__._mass_conversions = val

    @property
    def energy_conversions(self):
        return self.__class__._energy_conversions

    @energy_conversions.setter
    def energy_conversions(self, val):
        self.__class__._energy_conversions = val

    def __init__(self):
        self.length_units = 'R'
        self.length = 1.0
        self.lunit = self.length_units
        self.mass_units = 'R'
        self.mass = 1.0
        self.munit = self.mass_units
        self.energy_units = 'R'
        self.energy = 1.0
        self.Eunit = self.energy_units

    def set_length(self, new_length_units):
        self.length = self.length_conversions[new_length_units]
        self.length_units = new_length_units

    def set_mass(self, new_mass_units):
        self.mass = self.mass_conversions[new_mass_units]
        self.mass_units = new_mass_units

    def set_energy(self, new_energy_units):
        self.energy = self.energy_conversions[new_energy_units]
        self.energy_units = new_energy_units

    def get_length(self, val, unit):
        return val * self.length_conversions[self.length_units] / self.length_conversions[unit]

    def get_mass(self, val, unit):
        return val * self.mass_conversions[self.mass_units] / self.mass_conversions[unit]

    def get_energy(self, val, unit):
        return val * self.energy_conversions[self.energy_units] / self.energy_conversions[unit]

    def get_surface(self, val, unit):
        return val * (self.length_conversions[self.length_units] / self.length_conversions[unit]) ** 2.0

    def get_volume(self, val, unit):
        return val * (self.length_conversions[self.length_units] / self.length_conversions[unit]) ** 3.0

    def elem_charge(self):
        return 1.602e-19 / math.sqrt(4.0*math.pi*8.85e-12 * (1.0/self.get_length(1.0, 'nm'))
                                     * 1e-9 * (1.0 / self.get_energy(1.0, 'kJ/mol')) * 1e3 / 6.022e23)
